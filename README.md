# 5G Core Demo

## Presentation

[Link](https://docs.google.com/presentation/d/1vvhZa9UqtF3XdQtVXRO3CjkDeuA2r-XyARb-dxHXauA/)

## Software used

*  Java 1.8.0_201
*  Maven 3.3.3
*  Docker 18.09.3
*  Docker-Compose 1.23.2

## Build

```
mvn clean package
```

## Run Docker based setup

```
docker-compose up -d --build
```

## Test the setup

* Use HTTP client to invoke AMF emulator's UE Authentication Subflow
  * The request is available in postman format. See *test/5G NFs.postman_collection.json* file, *AMF Emulator: UE Authentication Subflow* item.
* See Jaeger tracing info for the invoked flow: http://localhost:16686/
* See Kibana console for HTTP traffic logs: http://localhost:5601/
  * Use *[fluentd-\*]* index pattern to get data comming from fluentd
  * Use *[log: "-----http_resp_start" or "-----http_req_start"]* filter to get HTTP log records
  
