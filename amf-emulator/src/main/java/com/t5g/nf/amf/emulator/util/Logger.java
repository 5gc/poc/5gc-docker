package com.t5g.nf.amf.emulator.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

public class Logger {
	
	public static void log(String... messages) {
		System.out.println("-----start-----");
		Arrays.stream(messages).forEach(s -> System.out.println(s));
		System.out.println("-----end-----");
	}
	
	public static void logHttpRequest(HttpRequest request, byte[] body) {
		System.out.println("-----http_req_start-----");
		System.out.println("URI         : " + request.getURI());
		System.out.println("Method      : " + request.getMethod());
		System.out.println("Headers     : " + request.getHeaders());
		try {
			System.out.println("Request body: " + new String(body, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			System.out.println("Logging error. Failed to log http request body");
			e.printStackTrace();
		}
		System.out.println("-----http_req_end-----");
	}
	
	public static void logHttpResponse(ClientHttpResponse response) {
		System.out.println("-----http_resp_start-----");
        try {
        	System.out.println("Status code  : " + response.getStatusCode());
	        System.out.println("Status text  : " + response.getStatusText());
	        System.out.println("Headers      : " + response.getHeaders());
	        System.out.println("Response body: " + StreamUtils.copyToString(response.getBody(), Charset.forName("UTF-8")));
        } catch (IOException e) {
        	System.out.println("Logging error. Failed to log http response body");
			e.printStackTrace();
        }
        System.out.println("-----http_resp_end-----");
    }

}
