package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnId;
import com.t5g.nf.nrf.nfdiscovery.model.TacRange;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TaiRange
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class TaiRange   {
  @JsonProperty("plmnId")
  private PlmnId plmnId;

  @JsonProperty("tacRangeList")
  @Valid
  private List<TacRange> tacRangeList = new ArrayList<>();

  public TaiRange plmnId(PlmnId plmnId) {
    this.plmnId = plmnId;
    return this;
  }

  /**
   * Get plmnId
   * @return plmnId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PlmnId getPlmnId() {
    return plmnId;
  }

  public void setPlmnId(PlmnId plmnId) {
    this.plmnId = plmnId;
  }

  public TaiRange tacRangeList(List<TacRange> tacRangeList) {
    this.tacRangeList = tacRangeList;
    return this;
  }

  public TaiRange addTacRangeListItem(TacRange tacRangeListItem) {
    this.tacRangeList.add(tacRangeListItem);
    return this;
  }

  /**
   * Get tacRangeList
   * @return tacRangeList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<TacRange> getTacRangeList() {
    return tacRangeList;
  }

  public void setTacRangeList(List<TacRange> tacRangeList) {
    this.tacRangeList = tacRangeList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TaiRange taiRange = (TaiRange) o;
    return Objects.equals(this.plmnId, taiRange.plmnId) &&
        Objects.equals(this.tacRangeList, taiRange.tacRangeList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(plmnId, tacRangeList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TaiRange {\n");
    
    sb.append("    plmnId: ").append(toIndentedString(plmnId)).append("\n");
    sb.append("    tacRangeList: ").append(toIndentedString(tacRangeList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

