package com.t5g.nf.amf.emulator.model;

public class AuthRequest {
	
	private String targetNfType;
	
	private String requesterNfType;
	
	private String supi;

	public String getTargetNfType() {
		return targetNfType;
	}

	public void setTargetNfType(String targetNfType) {
		this.targetNfType = targetNfType;
	}

	public String getRequesterNfType() {
		return requesterNfType;
	}

	public void setRequesterNfType(String requesterNfType) {
		this.requesterNfType = requesterNfType;
	}

	public String getSupi() {
		return supi;
	}

	public void setSupi(String supi) {
		this.supi = supi;
	}
	
}
