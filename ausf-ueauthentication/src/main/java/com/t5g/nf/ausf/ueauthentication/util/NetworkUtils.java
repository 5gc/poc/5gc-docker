package com.t5g.nf.ausf.ueauthentication.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NetworkUtils {

	@Value("${server.port}")
	private String port;

	@Value("${t5gc.nf.service.name}")
	private String serviceName;
	

	public String getRootUrl() throws UnknownHostException {
		String hostName = serviceName.isEmpty() ? InetAddress.getLocalHost().getHostName() : serviceName;
		
		return "http://" + hostName + ":" + port;
	}

}
