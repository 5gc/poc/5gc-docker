package com.t5g.nf.nrf.nfdiscovery.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

import com.t5g.nf.nrf.nfdiscovery.impl.AusfResponseProvider;
import com.t5g.nf.nrf.nfdiscovery.model.AccessType;
import com.t5g.nf.nrf.nfdiscovery.model.DataSetId;
import com.t5g.nf.nrf.nfdiscovery.model.IpEndPoint;
import com.t5g.nf.nrf.nfdiscovery.model.NFProfile;
import com.t5g.nf.nrf.nfdiscovery.model.NFService;
import com.t5g.nf.nrf.nfdiscovery.model.NFType;
import com.t5g.nf.nrf.nfdiscovery.model.PduSessionType;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnId;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnSnssai;
import com.t5g.nf.nrf.nfdiscovery.model.SearchResult;
import com.t5g.nf.nrf.nfdiscovery.model.ServiceName;
import com.t5g.nf.nrf.nfdiscovery.model.Snssai;
import com.t5g.nf.nrf.nfdiscovery.model.Tai;

import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

@Controller
@RequestMapping("${openapi.nRFNFDiscoveryService.base-path:/nnrf-disc/v1}")
public class NfInstancesApiController implements NfInstancesApi {
	
	@Autowired
	private AusfResponseProvider ausfResponseProvider;
	
    private final NativeWebRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public NfInstancesApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }
    
	@Override
	public ResponseEntity<SearchResult> searchNFInstances(@NotNull @ApiParam(value = "Type of the target NF", required = true, allowableValues = "NRF, UDM, AMF, SMF, AUSF, NEF, PCF, SMSF, NSSF, UDR, LMF, GMLC, 5G_EIR, SEPP, UPF, N3IWF, AF, UDSF, BSF, CHF, NWDAF") @Valid @RequestParam(value = "target-nf-type", required = true) NFType targetNfType,
			@NotNull @ApiParam(value = "Type of the requester NF", required = true, allowableValues = "NRF, UDM, AMF, SMF, AUSF, NEF, PCF, SMSF, NSSF, UDR, LMF, GMLC, 5G_EIR, SEPP, UPF, N3IWF, AF, UDSF, BSF, CHF, NWDAF") @Valid @RequestParam(value = "requester-nf-type", required = true) NFType requesterNfType,
			@Size(min = 1) @ApiParam(value = "Names of the services offered by the NF") @Valid @RequestParam(value = "service-names", required = false) List<ServiceName> serviceNames,
			@ApiParam(value = "FQDN of the requester NF") @Valid @RequestParam(value = "requester-nf-instance-fqdn", required = false) String requesterNfInstanceFqdn,
			@Size(min = 1) @ApiParam(value = "Id of the PLMN of the target NF") @Valid @RequestParam(value = "target-plmn-list", required = false) List<PlmnId> targetPlmnList,
			@Size(min = 1) @ApiParam(value = "Id of the PLMN where the NF issuing the Discovery request is located") @Valid @RequestParam(value = "requester-plmn-list", required = false) List<PlmnId> requesterPlmnList,
			@ApiParam(value = "Identity of the NF instance being discovered") @Valid @RequestParam(value = "target-nf-instance-id", required = false) UUID targetNfInstanceId,
			@ApiParam(value = "FQDN of the NF instance being discovered") @Valid @RequestParam(value = "target-nf-fqdn", required = false) String targetNfFqdn,
			@ApiParam(value = "Uri of the home NRF") @Valid @RequestParam(value = "hnrf-uri", required = false) String hnrfUri,
			@Size(min = 1) @ApiParam(value = "Slice info of the target NF") @Valid @RequestParam(value = "snssais", required = false) List<Snssai> snssais,
			@Size(min = 1) @ApiParam(value = "Slice info of the requester NF") @Valid @RequestParam(value = "requester-snssais", required = false) List<Snssai> requesterSnssais,
			@Size(min = 1) @ApiParam(value = "PLMN specific Slice info of the target NF") @Valid @RequestParam(value = "plmn-specific-snssai-list", required = false) List<PlmnSnssai> plmnSpecificSnssaiList,
			@ApiParam(value = "Dnn supported by the BSF, SMF or UPF") @Valid @RequestParam(value = "dnn", required = false) String dnn,
			@Size(min = 1) @ApiParam(value = "NSI IDs that are served by the services being discovered") @Valid @RequestParam(value = "nsi-list", required = false) List<String> nsiList,
			@ApiParam(value = "") @Valid @RequestParam(value = "smf-serving-area", required = false) String smfServingArea,
			/*@ApiParam(value = "Tracking Area Identity") @Valid Tai tai,*/
			/*Modified*/@ApiParam(value = "Tracking Area Identity") @Valid @RequestParam(value = "tai", required = false) Tai tai,
			@Pattern(regexp = "^[A-Fa-f0-9]{2}$") @ApiParam(value = "AMF Region Identity") @Valid @RequestParam(value = "amf-region-id", required = false) String amfRegionId,
			@Pattern(regexp = "^[0-3][A-Fa-f0-9]{2}$") @ApiParam(value = "AMF Set Identity") @Valid @RequestParam(value = "amf-set-id", required = false) String amfSetId,
			/*@ApiParam(value = "Guami used to search for an appropriate AMF") @Valid Guami guami,*/
			@Pattern(regexp = "^(imsi-[0-9]{5,15}|nai-.+|.+)$") @ApiParam(value = "SUPI of the user") @Valid @RequestParam(value = "supi", required = false) String supi,
			@Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$") @ApiParam(value = "IPv4 address of the UE") @Valid @RequestParam(value = "ue-ipv4-address", required = false) String ueIpv4Address,
			@ApiParam(value = "IP domain of the UE, which supported by BSF") @Valid @RequestParam(value = "ip-domain", required = false) String ipDomain,
			@ApiParam(value = "IPv6 prefix of the UE") @Valid String ueIpv6Prefix,
			@ApiParam(value = "Combined PGW-C and SMF or a standalone SMF") @Valid @RequestParam(value = "pgw-ind", required = false) Boolean pgwInd,
			@ApiParam(value = "PGW FQDN of a combined PGW-C and SMF") @Valid @RequestParam(value = "pgw", required = false) String pgw,
			@Pattern(regexp = "^(msisdn-[0-9]{5,15}|extid-[^@]+@[^@]+|.+)$") @ApiParam(value = "GPSI of the user") @Valid @RequestParam(value = "gpsi", required = false) String gpsi,
			@ApiParam(value = "external group identifier of the user") @Valid @RequestParam(value = "external-group-identity", required = false) String externalGroupIdentity,
			@ApiParam(value = "data set supported by the NF", defaultValue = "null") @Valid DataSetId dataSet,
			@Pattern(regexp = "^[0-9]{1,4}$") @ApiParam(value = "routing indicator in SUCI") @Valid @RequestParam(value = "routing-indicator", required = false) String routingIndicator,
			@Size(min = 1) @ApiParam(value = "Group IDs of the NFs being discovered") @Valid @RequestParam(value = "group-id-list", required = false) List<String> groupIdList,
			@Size(min = 1) @ApiParam(value = "Data network access identifiers of the NFs being discovered") @Valid @RequestParam(value = "dnai-list", required = false) List<String> dnaiList,
			@Size(min = 1) @ApiParam(value = "list of PDU Session Type required to be supported by the target NF") @Valid @RequestParam(value = "pdu-session-types", required = false) List<PduSessionType> pduSessionTypes,
			@Pattern(regexp = "^[A-Fa-f0-9]*$") @ApiParam(value = "Features required to be supported by the target NF") @Valid @RequestParam(value = "supported-features", required = false) String supportedFeatures,
			@ApiParam(value = "UPF supporting interworking with EPS or not") @Valid @RequestParam(value = "upf-iwk-eps-ind", required = false) Boolean upfIwkEpsInd,
			/*@ApiParam(value = "PLMN ID supported by a CHF") @Valid PlmnId chfSupportedPlmn, */
			/*Modified*/@ApiParam(value = "PLMN ID supported by a CHF") @Valid @RequestParam(value = "chf-supported-plmn", required = false) String chfSupportedPlmn,
			@ApiParam(value = "preferred target NF location") @Valid @RequestParam(value = "preferred-locality", required = false) String preferredLocality,
			@ApiParam(value = "AccessType supported by the target NF", allowableValues = "3GPP_ACCESS, NON_3GPP_ACCESS") @Valid @RequestParam(value = "access-type", required = false) AccessType accessType,
			@Min(1) @ApiParam(value = "Maximum number of NFProfiles to return in the response") @Valid @RequestParam(value = "limit", required = false) Integer limit,
			@Size(min = 1) @ApiParam(value = "Features required to be supported by the target NF") @Valid @RequestParam(value = "required-features", required = false) List<String> requiredFeatures,
			/*@ApiParam(value = "the complex query condition expression", defaultValue = "null") @Valid ComplexQuery complexQuery,*/
			@Max(2000) @ApiParam(value = "Maximum payload size of the response expressed in kilo octets", defaultValue = "124") @Valid @RequestParam(value = "max-payload-size", required = false, defaultValue = "124") Integer maxPayloadSize,
			@ApiParam(value = "Validator for conditional requests, as described in IETF RFC 7232, 3.2") @RequestHeader(value = "If-None-Match", required = false) String ifNoneMatch) {
		
		if (targetNfType == NFType.AUSF && requesterNfType == NFType.AMF && supi != null) {
			return ResponseEntity.ok().body(ausfResponseProvider.getData());
		} else {
			ResponseEntity<SearchResult> resp = ResponseEntity.badRequest().body(new SearchResult());
			return resp;
		}
		
	}
	
}
