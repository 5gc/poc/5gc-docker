package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.SupiRange;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PcfInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class PcfInfo   {
  @JsonProperty("dnnList")
  @Valid
  private List<String> dnnList = null;

  @JsonProperty("supiRanges")
  @Valid
  private List<SupiRange> supiRanges = null;

  @JsonProperty("rxDiamHost")
  private String rxDiamHost;

  @JsonProperty("rxDiamRealm")
  private String rxDiamRealm;

  public PcfInfo dnnList(List<String> dnnList) {
    this.dnnList = dnnList;
    return this;
  }

  public PcfInfo addDnnListItem(String dnnListItem) {
    if (this.dnnList == null) {
      this.dnnList = new ArrayList<>();
    }
    this.dnnList.add(dnnListItem);
    return this;
  }

  /**
   * Get dnnList
   * @return dnnList
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getDnnList() {
    return dnnList;
  }

  public void setDnnList(List<String> dnnList) {
    this.dnnList = dnnList;
  }

  public PcfInfo supiRanges(List<SupiRange> supiRanges) {
    this.supiRanges = supiRanges;
    return this;
  }

  public PcfInfo addSupiRangesItem(SupiRange supiRangesItem) {
    if (this.supiRanges == null) {
      this.supiRanges = new ArrayList<>();
    }
    this.supiRanges.add(supiRangesItem);
    return this;
  }

  /**
   * Get supiRanges
   * @return supiRanges
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<SupiRange> getSupiRanges() {
    return supiRanges;
  }

  public void setSupiRanges(List<SupiRange> supiRanges) {
    this.supiRanges = supiRanges;
  }

  public PcfInfo rxDiamHost(String rxDiamHost) {
    this.rxDiamHost = rxDiamHost;
    return this;
  }

  /**
   * Get rxDiamHost
   * @return rxDiamHost
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^([A-Za-z0-9]+([-A-Za-z0-9]+)\\.)+[a-z]{2,}$") 
  public String getRxDiamHost() {
    return rxDiamHost;
  }

  public void setRxDiamHost(String rxDiamHost) {
    this.rxDiamHost = rxDiamHost;
  }

  public PcfInfo rxDiamRealm(String rxDiamRealm) {
    this.rxDiamRealm = rxDiamRealm;
    return this;
  }

  /**
   * Get rxDiamRealm
   * @return rxDiamRealm
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^([A-Za-z0-9]+([-A-Za-z0-9]+)\\.)+[a-z]{2,}$") 
  public String getRxDiamRealm() {
    return rxDiamRealm;
  }

  public void setRxDiamRealm(String rxDiamRealm) {
    this.rxDiamRealm = rxDiamRealm;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PcfInfo pcfInfo = (PcfInfo) o;
    return Objects.equals(this.dnnList, pcfInfo.dnnList) &&
        Objects.equals(this.supiRanges, pcfInfo.supiRanges) &&
        Objects.equals(this.rxDiamHost, pcfInfo.rxDiamHost) &&
        Objects.equals(this.rxDiamRealm, pcfInfo.rxDiamRealm);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dnnList, supiRanges, rxDiamHost, rxDiamRealm);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PcfInfo {\n");
    
    sb.append("    dnnList: ").append(toIndentedString(dnnList)).append("\n");
    sb.append("    supiRanges: ").append(toIndentedString(supiRanges)).append("\n");
    sb.append("    rxDiamHost: ").append(toIndentedString(rxDiamHost)).append("\n");
    sb.append("    rxDiamRealm: ").append(toIndentedString(rxDiamRealm)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

