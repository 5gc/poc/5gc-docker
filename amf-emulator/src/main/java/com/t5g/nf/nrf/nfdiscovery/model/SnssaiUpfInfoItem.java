package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.DnnUpfInfoItem;
import com.t5g.nf.nrf.nfdiscovery.model.Snssai;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SnssaiUpfInfoItem
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class SnssaiUpfInfoItem   {
  @JsonProperty("sNssai")
  private Snssai sNssai;

  @JsonProperty("dnnUpfInfoList")
  @Valid
  private List<DnnUpfInfoItem> dnnUpfInfoList = new ArrayList<>();

  public SnssaiUpfInfoItem sNssai(Snssai sNssai) {
    this.sNssai = sNssai;
    return this;
  }

  /**
   * Get sNssai
   * @return sNssai
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Snssai getsNssai() {
    return sNssai;
  }

  public void setsNssai(Snssai sNssai) {
    this.sNssai = sNssai;
  }

  public SnssaiUpfInfoItem dnnUpfInfoList(List<DnnUpfInfoItem> dnnUpfInfoList) {
    this.dnnUpfInfoList = dnnUpfInfoList;
    return this;
  }

  public SnssaiUpfInfoItem addDnnUpfInfoListItem(DnnUpfInfoItem dnnUpfInfoListItem) {
    this.dnnUpfInfoList.add(dnnUpfInfoListItem);
    return this;
  }

  /**
   * Get dnnUpfInfoList
   * @return dnnUpfInfoList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<DnnUpfInfoItem> getDnnUpfInfoList() {
    return dnnUpfInfoList;
  }

  public void setDnnUpfInfoList(List<DnnUpfInfoItem> dnnUpfInfoList) {
    this.dnnUpfInfoList = dnnUpfInfoList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SnssaiUpfInfoItem snssaiUpfInfoItem = (SnssaiUpfInfoItem) o;
    return Objects.equals(this.sNssai, snssaiUpfInfoItem.sNssai) &&
        Objects.equals(this.dnnUpfInfoList, snssaiUpfInfoItem.dnnUpfInfoList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sNssai, dnnUpfInfoList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SnssaiUpfInfoItem {\n");
    
    sb.append("    sNssai: ").append(toIndentedString(sNssai)).append("\n");
    sb.append("    dnnUpfInfoList: ").append(toIndentedString(dnnUpfInfoList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

