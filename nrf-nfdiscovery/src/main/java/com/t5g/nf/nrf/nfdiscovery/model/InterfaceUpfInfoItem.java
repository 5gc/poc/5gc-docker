package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.UPInterfaceType;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * InterfaceUpfInfoItem
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class InterfaceUpfInfoItem   {
  @JsonProperty("interfaceType")
  private UPInterfaceType interfaceType = null;

  @JsonProperty("ipv4EndpointAddresses")
  @Valid
  private List<String> ipv4EndpointAddresses = null;

  @JsonProperty("ipv6EndpointAddresses")
  @Valid
  private List<String> ipv6EndpointAddresses = null;

  @JsonProperty("endpointFqdn")
  private String endpointFqdn;

  @JsonProperty("networkInstance")
  private String networkInstance;

  public InterfaceUpfInfoItem interfaceType(UPInterfaceType interfaceType) {
    this.interfaceType = interfaceType;
    return this;
  }

  /**
   * Get interfaceType
   * @return interfaceType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public UPInterfaceType getInterfaceType() {
    return interfaceType;
  }

  public void setInterfaceType(UPInterfaceType interfaceType) {
    this.interfaceType = interfaceType;
  }

  public InterfaceUpfInfoItem ipv4EndpointAddresses(List<String> ipv4EndpointAddresses) {
    this.ipv4EndpointAddresses = ipv4EndpointAddresses;
    return this;
  }

  public InterfaceUpfInfoItem addIpv4EndpointAddressesItem(String ipv4EndpointAddressesItem) {
    if (this.ipv4EndpointAddresses == null) {
      this.ipv4EndpointAddresses = new ArrayList<>();
    }
    this.ipv4EndpointAddresses.add(ipv4EndpointAddressesItem);
    return this;
  }

  /**
   * Get ipv4EndpointAddresses
   * @return ipv4EndpointAddresses
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getIpv4EndpointAddresses() {
    return ipv4EndpointAddresses;
  }

  public void setIpv4EndpointAddresses(List<String> ipv4EndpointAddresses) {
    this.ipv4EndpointAddresses = ipv4EndpointAddresses;
  }

  public InterfaceUpfInfoItem ipv6EndpointAddresses(List<String> ipv6EndpointAddresses) {
    this.ipv6EndpointAddresses = ipv6EndpointAddresses;
    return this;
  }

  public InterfaceUpfInfoItem addIpv6EndpointAddressesItem(String ipv6EndpointAddressesItem) {
    if (this.ipv6EndpointAddresses == null) {
      this.ipv6EndpointAddresses = new ArrayList<>();
    }
    this.ipv6EndpointAddresses.add(ipv6EndpointAddressesItem);
    return this;
  }

  /**
   * Get ipv6EndpointAddresses
   * @return ipv6EndpointAddresses
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getIpv6EndpointAddresses() {
    return ipv6EndpointAddresses;
  }

  public void setIpv6EndpointAddresses(List<String> ipv6EndpointAddresses) {
    this.ipv6EndpointAddresses = ipv6EndpointAddresses;
  }

  public InterfaceUpfInfoItem endpointFqdn(String endpointFqdn) {
    this.endpointFqdn = endpointFqdn;
    return this;
  }

  /**
   * Get endpointFqdn
   * @return endpointFqdn
  */
  @ApiModelProperty(value = "")


  public String getEndpointFqdn() {
    return endpointFqdn;
  }

  public void setEndpointFqdn(String endpointFqdn) {
    this.endpointFqdn = endpointFqdn;
  }

  public InterfaceUpfInfoItem networkInstance(String networkInstance) {
    this.networkInstance = networkInstance;
    return this;
  }

  /**
   * Get networkInstance
   * @return networkInstance
  */
  @ApiModelProperty(value = "")


  public String getNetworkInstance() {
    return networkInstance;
  }

  public void setNetworkInstance(String networkInstance) {
    this.networkInstance = networkInstance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InterfaceUpfInfoItem interfaceUpfInfoItem = (InterfaceUpfInfoItem) o;
    return Objects.equals(this.interfaceType, interfaceUpfInfoItem.interfaceType) &&
        Objects.equals(this.ipv4EndpointAddresses, interfaceUpfInfoItem.ipv4EndpointAddresses) &&
        Objects.equals(this.ipv6EndpointAddresses, interfaceUpfInfoItem.ipv6EndpointAddresses) &&
        Objects.equals(this.endpointFqdn, interfaceUpfInfoItem.endpointFqdn) &&
        Objects.equals(this.networkInstance, interfaceUpfInfoItem.networkInstance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(interfaceType, ipv4EndpointAddresses, ipv6EndpointAddresses, endpointFqdn, networkInstance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InterfaceUpfInfoItem {\n");
    
    sb.append("    interfaceType: ").append(toIndentedString(interfaceType)).append("\n");
    sb.append("    ipv4EndpointAddresses: ").append(toIndentedString(ipv4EndpointAddresses)).append("\n");
    sb.append("    ipv6EndpointAddresses: ").append(toIndentedString(ipv6EndpointAddresses)).append("\n");
    sb.append("    endpointFqdn: ").append(toIndentedString(endpointFqdn)).append("\n");
    sb.append("    networkInstance: ").append(toIndentedString(networkInstance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

