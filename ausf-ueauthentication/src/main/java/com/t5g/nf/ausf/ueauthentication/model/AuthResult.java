package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets AuthResult
 */
public enum AuthResult {
  
  SUCCESS("AUTHENTICATION_SUCCESS"),
  
  FAILURE("AUTHENTICATION_FAILURE"),
  
  ONGOING("AUTHENTICATION_ONGOING");

  private String value;

  AuthResult(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AuthResult fromValue(String value) {
    for (AuthResult b : AuthResult.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

