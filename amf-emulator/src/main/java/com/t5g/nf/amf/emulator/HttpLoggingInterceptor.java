package com.t5g.nf.amf.emulator;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.t5g.nf.amf.emulator.util.Logger;

public class HttpLoggingInterceptor implements ClientHttpRequestInterceptor {

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		logRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		logResponse(response);
		
		return response;
	}

	private void logRequest(HttpRequest request, byte[] body) {
		Logger.logHttpRequest(request, body);
	}

	private void logResponse(ClientHttpResponse response) {
		Logger.logHttpResponse(response);
	}

}
