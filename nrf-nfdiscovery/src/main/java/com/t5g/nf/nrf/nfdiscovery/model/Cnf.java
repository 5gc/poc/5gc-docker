package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.CnfUnit;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Cnf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class Cnf   {
  @JsonProperty("cnfUnits")
  @Valid
  private List<CnfUnit> cnfUnits = new ArrayList<>();

  public Cnf cnfUnits(List<CnfUnit> cnfUnits) {
    this.cnfUnits = cnfUnits;
    return this;
  }

  public Cnf addCnfUnitsItem(CnfUnit cnfUnitsItem) {
    this.cnfUnits.add(cnfUnitsItem);
    return this;
  }

  /**
   * Get cnfUnits
   * @return cnfUnits
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<CnfUnit> getCnfUnits() {
    return cnfUnits;
  }

  public void setCnfUnits(List<CnfUnit> cnfUnits) {
    this.cnfUnits = cnfUnits;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Cnf cnf = (Cnf) o;
    return Objects.equals(this.cnfUnits, cnf.cnfUnits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cnfUnits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Cnf {\n");
    
    sb.append("    cnfUnits: ").append(toIndentedString(cnfUnits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

