package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.ChfServiceInfo;
import com.t5g.nf.nrf.nfdiscovery.model.DefaultNotificationSubscription;
import com.t5g.nf.nrf.nfdiscovery.model.IpEndPoint;
import com.t5g.nf.nrf.nfdiscovery.model.NFServiceStatus;
import com.t5g.nf.nrf.nfdiscovery.model.NFServiceVersion;
import com.t5g.nf.nrf.nfdiscovery.model.ServiceName;
import com.t5g.nf.nrf.nfdiscovery.model.UriScheme;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NFService
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class NFService   {
  @JsonProperty("serviceInstanceId")
  private String serviceInstanceId;

  @JsonProperty("serviceName")
  private ServiceName serviceName = null;

  @JsonProperty("versions")
  @Valid
  private List<NFServiceVersion> versions = new ArrayList<>();

  @JsonProperty("scheme")
  private UriScheme scheme = null;

  @JsonProperty("nfServiceStatus")
  private NFServiceStatus nfServiceStatus = null;

  @JsonProperty("fqdn")
  private String fqdn;

  @JsonProperty("ipEndPoints")
  @Valid
  private List<IpEndPoint> ipEndPoints = null;

  @JsonProperty("apiPrefix")
  private String apiPrefix;

  @JsonProperty("defaultNotificationSubscriptions")
  @Valid
  private List<DefaultNotificationSubscription> defaultNotificationSubscriptions = null;

  @JsonProperty("capacity")
  private Integer capacity;

  @JsonProperty("load")
  private Integer load;

  @JsonProperty("priority")
  private Integer priority;

  @JsonProperty("recoveryTime")
  private OffsetDateTime recoveryTime;

  @JsonProperty("chfServiceInfo")
  private ChfServiceInfo chfServiceInfo;

  @JsonProperty("supportedFeatures")
  private String supportedFeatures;

  public NFService serviceInstanceId(String serviceInstanceId) {
    this.serviceInstanceId = serviceInstanceId;
    return this;
  }

  /**
   * Get serviceInstanceId
   * @return serviceInstanceId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getServiceInstanceId() {
    return serviceInstanceId;
  }

  public void setServiceInstanceId(String serviceInstanceId) {
    this.serviceInstanceId = serviceInstanceId;
  }

  public NFService serviceName(ServiceName serviceName) {
    this.serviceName = serviceName;
    return this;
  }

  /**
   * Get serviceName
   * @return serviceName
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ServiceName getServiceName() {
    return serviceName;
  }

  public void setServiceName(ServiceName serviceName) {
    this.serviceName = serviceName;
  }

  public NFService versions(List<NFServiceVersion> versions) {
    this.versions = versions;
    return this;
  }

  public NFService addVersionsItem(NFServiceVersion versionsItem) {
    this.versions.add(versionsItem);
    return this;
  }

  /**
   * Get versions
   * @return versions
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<NFServiceVersion> getVersions() {
    return versions;
  }

  public void setVersions(List<NFServiceVersion> versions) {
    this.versions = versions;
  }

  public NFService scheme(UriScheme scheme) {
    this.scheme = scheme;
    return this;
  }

  /**
   * Get scheme
   * @return scheme
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public UriScheme getScheme() {
    return scheme;
  }

  public void setScheme(UriScheme scheme) {
    this.scheme = scheme;
  }

  public NFService nfServiceStatus(NFServiceStatus nfServiceStatus) {
    this.nfServiceStatus = nfServiceStatus;
    return this;
  }

  /**
   * Get nfServiceStatus
   * @return nfServiceStatus
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public NFServiceStatus getNfServiceStatus() {
    return nfServiceStatus;
  }

  public void setNfServiceStatus(NFServiceStatus nfServiceStatus) {
    this.nfServiceStatus = nfServiceStatus;
  }

  public NFService fqdn(String fqdn) {
    this.fqdn = fqdn;
    return this;
  }

  /**
   * Get fqdn
   * @return fqdn
  */
  @ApiModelProperty(value = "")


  public String getFqdn() {
    return fqdn;
  }

  public void setFqdn(String fqdn) {
    this.fqdn = fqdn;
  }

  public NFService ipEndPoints(List<IpEndPoint> ipEndPoints) {
    this.ipEndPoints = ipEndPoints;
    return this;
  }

  public NFService addIpEndPointsItem(IpEndPoint ipEndPointsItem) {
    if (this.ipEndPoints == null) {
      this.ipEndPoints = new ArrayList<>();
    }
    this.ipEndPoints.add(ipEndPointsItem);
    return this;
  }

  /**
   * Get ipEndPoints
   * @return ipEndPoints
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<IpEndPoint> getIpEndPoints() {
    return ipEndPoints;
  }

  public void setIpEndPoints(List<IpEndPoint> ipEndPoints) {
    this.ipEndPoints = ipEndPoints;
  }

  public NFService apiPrefix(String apiPrefix) {
    this.apiPrefix = apiPrefix;
    return this;
  }

  /**
   * Get apiPrefix
   * @return apiPrefix
  */
  @ApiModelProperty(value = "")


  public String getApiPrefix() {
    return apiPrefix;
  }

  public void setApiPrefix(String apiPrefix) {
    this.apiPrefix = apiPrefix;
  }

  public NFService defaultNotificationSubscriptions(List<DefaultNotificationSubscription> defaultNotificationSubscriptions) {
    this.defaultNotificationSubscriptions = defaultNotificationSubscriptions;
    return this;
  }

  public NFService addDefaultNotificationSubscriptionsItem(DefaultNotificationSubscription defaultNotificationSubscriptionsItem) {
    if (this.defaultNotificationSubscriptions == null) {
      this.defaultNotificationSubscriptions = new ArrayList<>();
    }
    this.defaultNotificationSubscriptions.add(defaultNotificationSubscriptionsItem);
    return this;
  }

  /**
   * Get defaultNotificationSubscriptions
   * @return defaultNotificationSubscriptions
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<DefaultNotificationSubscription> getDefaultNotificationSubscriptions() {
    return defaultNotificationSubscriptions;
  }

  public void setDefaultNotificationSubscriptions(List<DefaultNotificationSubscription> defaultNotificationSubscriptions) {
    this.defaultNotificationSubscriptions = defaultNotificationSubscriptions;
  }

  public NFService capacity(Integer capacity) {
    this.capacity = capacity;
    return this;
  }

  /**
   * Get capacity
   * minimum: 0
   * maximum: 65535
   * @return capacity
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(65535) 
  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public NFService load(Integer load) {
    this.load = load;
    return this;
  }

  /**
   * Get load
   * minimum: 0
   * maximum: 100
   * @return load
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(100) 
  public Integer getLoad() {
    return load;
  }

  public void setLoad(Integer load) {
    this.load = load;
  }

  public NFService priority(Integer priority) {
    this.priority = priority;
    return this;
  }

  /**
   * Get priority
   * minimum: 0
   * maximum: 65535
   * @return priority
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(65535) 
  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public NFService recoveryTime(OffsetDateTime recoveryTime) {
    this.recoveryTime = recoveryTime;
    return this;
  }

  /**
   * Get recoveryTime
   * @return recoveryTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getRecoveryTime() {
    return recoveryTime;
  }

  public void setRecoveryTime(OffsetDateTime recoveryTime) {
    this.recoveryTime = recoveryTime;
  }

  public NFService chfServiceInfo(ChfServiceInfo chfServiceInfo) {
    this.chfServiceInfo = chfServiceInfo;
    return this;
  }

  /**
   * Get chfServiceInfo
   * @return chfServiceInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public ChfServiceInfo getChfServiceInfo() {
    return chfServiceInfo;
  }

  public void setChfServiceInfo(ChfServiceInfo chfServiceInfo) {
    this.chfServiceInfo = chfServiceInfo;
  }

  public NFService supportedFeatures(String supportedFeatures) {
    this.supportedFeatures = supportedFeatures;
    return this;
  }

  /**
   * Get supportedFeatures
   * @return supportedFeatures
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^[A-Fa-f0-9]*$") 
  public String getSupportedFeatures() {
    return supportedFeatures;
  }

  public void setSupportedFeatures(String supportedFeatures) {
    this.supportedFeatures = supportedFeatures;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NFService nfService = (NFService) o;
    return Objects.equals(this.serviceInstanceId, nfService.serviceInstanceId) &&
        Objects.equals(this.serviceName, nfService.serviceName) &&
        Objects.equals(this.versions, nfService.versions) &&
        Objects.equals(this.scheme, nfService.scheme) &&
        Objects.equals(this.nfServiceStatus, nfService.nfServiceStatus) &&
        Objects.equals(this.fqdn, nfService.fqdn) &&
        Objects.equals(this.ipEndPoints, nfService.ipEndPoints) &&
        Objects.equals(this.apiPrefix, nfService.apiPrefix) &&
        Objects.equals(this.defaultNotificationSubscriptions, nfService.defaultNotificationSubscriptions) &&
        Objects.equals(this.capacity, nfService.capacity) &&
        Objects.equals(this.load, nfService.load) &&
        Objects.equals(this.priority, nfService.priority) &&
        Objects.equals(this.recoveryTime, nfService.recoveryTime) &&
        Objects.equals(this.chfServiceInfo, nfService.chfServiceInfo) &&
        Objects.equals(this.supportedFeatures, nfService.supportedFeatures);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceInstanceId, serviceName, versions, scheme, nfServiceStatus, fqdn, ipEndPoints, apiPrefix, defaultNotificationSubscriptions, capacity, load, priority, recoveryTime, chfServiceInfo, supportedFeatures);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NFService {\n");
    
    sb.append("    serviceInstanceId: ").append(toIndentedString(serviceInstanceId)).append("\n");
    sb.append("    serviceName: ").append(toIndentedString(serviceName)).append("\n");
    sb.append("    versions: ").append(toIndentedString(versions)).append("\n");
    sb.append("    scheme: ").append(toIndentedString(scheme)).append("\n");
    sb.append("    nfServiceStatus: ").append(toIndentedString(nfServiceStatus)).append("\n");
    sb.append("    fqdn: ").append(toIndentedString(fqdn)).append("\n");
    sb.append("    ipEndPoints: ").append(toIndentedString(ipEndPoints)).append("\n");
    sb.append("    apiPrefix: ").append(toIndentedString(apiPrefix)).append("\n");
    sb.append("    defaultNotificationSubscriptions: ").append(toIndentedString(defaultNotificationSubscriptions)).append("\n");
    sb.append("    capacity: ").append(toIndentedString(capacity)).append("\n");
    sb.append("    load: ").append(toIndentedString(load)).append("\n");
    sb.append("    priority: ").append(toIndentedString(priority)).append("\n");
    sb.append("    recoveryTime: ").append(toIndentedString(recoveryTime)).append("\n");
    sb.append("    chfServiceInfo: ").append(toIndentedString(chfServiceInfo)).append("\n");
    sb.append("    supportedFeatures: ").append(toIndentedString(supportedFeatures)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

