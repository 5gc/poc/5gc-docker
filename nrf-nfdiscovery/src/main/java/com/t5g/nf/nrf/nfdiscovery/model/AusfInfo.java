package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.SupiRange;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AusfInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class AusfInfo   {
  @JsonProperty("groupId")
  private String groupId;

  @JsonProperty("supiRanges")
  @Valid
  private List<SupiRange> supiRanges = null;

  @JsonProperty("routingIndicators")
  @Valid
  private List<String> routingIndicators = null;

  public AusfInfo groupId(String groupId) {
    this.groupId = groupId;
    return this;
  }

  /**
   * Get groupId
   * @return groupId
  */
  @ApiModelProperty(value = "")


  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public AusfInfo supiRanges(List<SupiRange> supiRanges) {
    this.supiRanges = supiRanges;
    return this;
  }

  public AusfInfo addSupiRangesItem(SupiRange supiRangesItem) {
    if (this.supiRanges == null) {
      this.supiRanges = new ArrayList<>();
    }
    this.supiRanges.add(supiRangesItem);
    return this;
  }

  /**
   * Get supiRanges
   * @return supiRanges
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<SupiRange> getSupiRanges() {
    return supiRanges;
  }

  public void setSupiRanges(List<SupiRange> supiRanges) {
    this.supiRanges = supiRanges;
  }

  public AusfInfo routingIndicators(List<String> routingIndicators) {
    this.routingIndicators = routingIndicators;
    return this;
  }

  public AusfInfo addRoutingIndicatorsItem(String routingIndicatorsItem) {
    if (this.routingIndicators == null) {
      this.routingIndicators = new ArrayList<>();
    }
    this.routingIndicators.add(routingIndicatorsItem);
    return this;
  }

  /**
   * Get routingIndicators
   * @return routingIndicators
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getRoutingIndicators() {
    return routingIndicators;
  }

  public void setRoutingIndicators(List<String> routingIndicators) {
    this.routingIndicators = routingIndicators;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AusfInfo ausfInfo = (AusfInfo) o;
    return Objects.equals(this.groupId, ausfInfo.groupId) &&
        Objects.equals(this.supiRanges, ausfInfo.supiRanges) &&
        Objects.equals(this.routingIndicators, ausfInfo.routingIndicators);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupId, supiRanges, routingIndicators);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AusfInfo {\n");
    
    sb.append("    groupId: ").append(toIndentedString(groupId)).append("\n");
    sb.append("    supiRanges: ").append(toIndentedString(supiRanges)).append("\n");
    sb.append("    routingIndicators: ").append(toIndentedString(routingIndicators)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

