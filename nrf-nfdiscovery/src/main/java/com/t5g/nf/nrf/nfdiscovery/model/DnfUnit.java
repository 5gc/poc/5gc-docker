package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.Atom;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DnfUnit
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class DnfUnit   {
  @JsonProperty("dnfUnit")
  @Valid
  private List<Atom> dnfUnit = new ArrayList<>();

  public DnfUnit dnfUnit(List<Atom> dnfUnit) {
    this.dnfUnit = dnfUnit;
    return this;
  }

  public DnfUnit addDnfUnitItem(Atom dnfUnitItem) {
    this.dnfUnit.add(dnfUnitItem);
    return this;
  }

  /**
   * Get dnfUnit
   * @return dnfUnit
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<Atom> getDnfUnit() {
    return dnfUnit;
  }

  public void setDnfUnit(List<Atom> dnfUnit) {
    this.dnfUnit = dnfUnit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DnfUnit dnfUnit = (DnfUnit) o;
    return Objects.equals(this.dnfUnit, dnfUnit.dnfUnit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dnfUnit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DnfUnit {\n");
    
    sb.append("    dnfUnit: ").append(toIndentedString(dnfUnit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

