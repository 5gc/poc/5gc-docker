package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets NFType
 */
public enum NFType {
  
  NRF("NRF"),
  
  UDM("UDM"),
  
  AMF("AMF"),
  
  SMF("SMF"),
  
  AUSF("AUSF"),
  
  NEF("NEF"),
  
  PCF("PCF"),
  
  SMSF("SMSF"),
  
  NSSF("NSSF"),
  
  UDR("UDR"),
  
  LMF("LMF"),
  
  GMLC("GMLC"),
  
  _5G_EIR("5G_EIR"),
  
  SEPP("SEPP"),
  
  UPF("UPF"),
  
  N3IWF("N3IWF"),
  
  AF("AF"),
  
  UDSF("UDSF"),
  
  BSF("BSF"),
  
  CHF("CHF"),
  
  NWDAF("NWDAF");

  private String value;

  NFType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static NFType fromValue(String value) {
    for (NFType b : NFType.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

