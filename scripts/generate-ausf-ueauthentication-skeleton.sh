#!/bin/sh

java -jar openapi-generator-cli.jar generate \
  -i TS29509_Nausf_UEAuthentication.yaml \
  -g spring \
  --invoker-package com.t5g.nf.ausf.ueauthentication \
  --api-package com.t5g.nf.ausf.ueauthentication.api \
  --model-package com.t5g.nf.ausf.ueauthentication.model \
  --group-id com.t5g.nf \
  --artifact-id ausf-ueauthentication \
  --artifact-version 0.0.1-SNAPSHOT \
  -o ausf-ueauthentication