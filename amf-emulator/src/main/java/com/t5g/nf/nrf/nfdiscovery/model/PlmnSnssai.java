package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnId;
import com.t5g.nf.nrf.nfdiscovery.model.Snssai;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PlmnSnssai
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class PlmnSnssai   {
  @JsonProperty("plmnId")
  private PlmnId plmnId;

  @JsonProperty("sNssaiList")
  @Valid
  private List<Snssai> sNssaiList = new ArrayList<>();

  public PlmnSnssai plmnId(PlmnId plmnId) {
    this.plmnId = plmnId;
    return this;
  }

  /**
   * Get plmnId
   * @return plmnId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PlmnId getPlmnId() {
    return plmnId;
  }

  public void setPlmnId(PlmnId plmnId) {
    this.plmnId = plmnId;
  }

  public PlmnSnssai sNssaiList(List<Snssai> sNssaiList) {
    this.sNssaiList = sNssaiList;
    return this;
  }

  public PlmnSnssai addSNssaiListItem(Snssai sNssaiListItem) {
    this.sNssaiList.add(sNssaiListItem);
    return this;
  }

  /**
   * Get sNssaiList
   * @return sNssaiList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<Snssai> getsNssaiList() {
    return sNssaiList;
  }

  public void setsNssaiList(List<Snssai> sNssaiList) {
    this.sNssaiList = sNssaiList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PlmnSnssai plmnSnssai = (PlmnSnssai) o;
    return Objects.equals(this.plmnId, plmnSnssai.plmnId) &&
        Objects.equals(this.sNssaiList, plmnSnssai.sNssaiList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(plmnId, sNssaiList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PlmnSnssai {\n");
    
    sb.append("    plmnId: ").append(toIndentedString(plmnId)).append("\n");
    sb.append("    sNssaiList: ").append(toIndentedString(sNssaiList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

