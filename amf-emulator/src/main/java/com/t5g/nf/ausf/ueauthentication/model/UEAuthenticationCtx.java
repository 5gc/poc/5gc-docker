package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.ausf.ueauthentication.model.AuthType;
import com.t5g.nf.ausf.ueauthentication.model.Av5gAka;
import com.t5g.nf.ausf.ueauthentication.model.LinksValueSchema;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UEAuthenticationCtx
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class UEAuthenticationCtx   {
  @JsonProperty("authType")
  private AuthType authType;

  @JsonProperty("5gAuthData")
  private Av5gAka _5gAuthData;

  @JsonProperty("_links")
  @Valid
  private Map<String, LinksValueSchema> links = new HashMap<>();

  @JsonProperty("servingNetworkName")
  private String servingNetworkName;

  public UEAuthenticationCtx authType(AuthType authType) {
    this.authType = authType;
    return this;
  }

  /**
   * Get authType
   * @return authType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthType getAuthType() {
    return authType;
  }

  public void setAuthType(AuthType authType) {
    this.authType = authType;
  }

  public UEAuthenticationCtx _5gAuthData(Av5gAka _5gAuthData) {
    this._5gAuthData = _5gAuthData;
    return this;
  }

  /**
   * Get _5gAuthData
   * @return _5gAuthData
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Av5gAka get5gAuthData() {
    return _5gAuthData;
  }

  public void set5gAuthData(Av5gAka _5gAuthData) {
    this._5gAuthData = _5gAuthData;
  }

  public UEAuthenticationCtx links(Map<String, LinksValueSchema> links) {
    this.links = links;
    return this;
  }

  public UEAuthenticationCtx putLinksItem(String key, LinksValueSchema linksItem) {
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Map<String, LinksValueSchema> getLinks() {
    return links;
  }

  public void setLinks(Map<String, LinksValueSchema> links) {
    this.links = links;
  }

  public UEAuthenticationCtx servingNetworkName(String servingNetworkName) {
    this.servingNetworkName = servingNetworkName;
    return this;
  }

  /**
   * Get servingNetworkName
   * @return servingNetworkName
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^5G:mnc[0-9]{3}[.]mcc[0-9]{3}[.]3gppnetwork[.]org$") 
  public String getServingNetworkName() {
    return servingNetworkName;
  }

  public void setServingNetworkName(String servingNetworkName) {
    this.servingNetworkName = servingNetworkName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UEAuthenticationCtx ueAuthenticationCtx = (UEAuthenticationCtx) o;
    return Objects.equals(this.authType, ueAuthenticationCtx.authType) &&
        Objects.equals(this._5gAuthData, ueAuthenticationCtx._5gAuthData) &&
        Objects.equals(this.links, ueAuthenticationCtx.links) &&
        Objects.equals(this.servingNetworkName, ueAuthenticationCtx.servingNetworkName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authType, _5gAuthData, links, servingNetworkName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UEAuthenticationCtx {\n");
    
    sb.append("    authType: ").append(toIndentedString(authType)).append("\n");
    sb.append("    _5gAuthData: ").append(toIndentedString(_5gAuthData)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    servingNetworkName: ").append(toIndentedString(servingNetworkName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

