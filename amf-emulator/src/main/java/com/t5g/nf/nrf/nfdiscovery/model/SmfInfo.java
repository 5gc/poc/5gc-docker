package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.AccessType;
import com.t5g.nf.nrf.nfdiscovery.model.SnssaiSmfInfoItem;
import com.t5g.nf.nrf.nfdiscovery.model.Tai;
import com.t5g.nf.nrf.nfdiscovery.model.TaiRange;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SmfInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class SmfInfo   {
  @JsonProperty("sNssaiSmfInfoList")
  @Valid
  private List<SnssaiSmfInfoItem> sNssaiSmfInfoList = new ArrayList<>();

  @JsonProperty("taiList")
  @Valid
  private List<Tai> taiList = null;

  @JsonProperty("taiRangeList")
  @Valid
  private List<TaiRange> taiRangeList = null;

  @JsonProperty("pgwFqdn")
  private String pgwFqdn;

  @JsonProperty("accessType")
  @Valid
  private List<AccessType> accessType = null;

  public SmfInfo sNssaiSmfInfoList(List<SnssaiSmfInfoItem> sNssaiSmfInfoList) {
    this.sNssaiSmfInfoList = sNssaiSmfInfoList;
    return this;
  }

  public SmfInfo addSNssaiSmfInfoListItem(SnssaiSmfInfoItem sNssaiSmfInfoListItem) {
    this.sNssaiSmfInfoList.add(sNssaiSmfInfoListItem);
    return this;
  }

  /**
   * Get sNssaiSmfInfoList
   * @return sNssaiSmfInfoList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<SnssaiSmfInfoItem> getsNssaiSmfInfoList() {
    return sNssaiSmfInfoList;
  }

  public void setsNssaiSmfInfoList(List<SnssaiSmfInfoItem> sNssaiSmfInfoList) {
    this.sNssaiSmfInfoList = sNssaiSmfInfoList;
  }

  public SmfInfo taiList(List<Tai> taiList) {
    this.taiList = taiList;
    return this;
  }

  public SmfInfo addTaiListItem(Tai taiListItem) {
    if (this.taiList == null) {
      this.taiList = new ArrayList<>();
    }
    this.taiList.add(taiListItem);
    return this;
  }

  /**
   * Get taiList
   * @return taiList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<Tai> getTaiList() {
    return taiList;
  }

  public void setTaiList(List<Tai> taiList) {
    this.taiList = taiList;
  }

  public SmfInfo taiRangeList(List<TaiRange> taiRangeList) {
    this.taiRangeList = taiRangeList;
    return this;
  }

  public SmfInfo addTaiRangeListItem(TaiRange taiRangeListItem) {
    if (this.taiRangeList == null) {
      this.taiRangeList = new ArrayList<>();
    }
    this.taiRangeList.add(taiRangeListItem);
    return this;
  }

  /**
   * Get taiRangeList
   * @return taiRangeList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<TaiRange> getTaiRangeList() {
    return taiRangeList;
  }

  public void setTaiRangeList(List<TaiRange> taiRangeList) {
    this.taiRangeList = taiRangeList;
  }

  public SmfInfo pgwFqdn(String pgwFqdn) {
    this.pgwFqdn = pgwFqdn;
    return this;
  }

  /**
   * Get pgwFqdn
   * @return pgwFqdn
  */
  @ApiModelProperty(value = "")


  public String getPgwFqdn() {
    return pgwFqdn;
  }

  public void setPgwFqdn(String pgwFqdn) {
    this.pgwFqdn = pgwFqdn;
  }

  public SmfInfo accessType(List<AccessType> accessType) {
    this.accessType = accessType;
    return this;
  }

  public SmfInfo addAccessTypeItem(AccessType accessTypeItem) {
    if (this.accessType == null) {
      this.accessType = new ArrayList<>();
    }
    this.accessType.add(accessTypeItem);
    return this;
  }

  /**
   * Get accessType
   * @return accessType
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<AccessType> getAccessType() {
    return accessType;
  }

  public void setAccessType(List<AccessType> accessType) {
    this.accessType = accessType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SmfInfo smfInfo = (SmfInfo) o;
    return Objects.equals(this.sNssaiSmfInfoList, smfInfo.sNssaiSmfInfoList) &&
        Objects.equals(this.taiList, smfInfo.taiList) &&
        Objects.equals(this.taiRangeList, smfInfo.taiRangeList) &&
        Objects.equals(this.pgwFqdn, smfInfo.pgwFqdn) &&
        Objects.equals(this.accessType, smfInfo.accessType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sNssaiSmfInfoList, taiList, taiRangeList, pgwFqdn, accessType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SmfInfo {\n");
    
    sb.append("    sNssaiSmfInfoList: ").append(toIndentedString(sNssaiSmfInfoList)).append("\n");
    sb.append("    taiList: ").append(toIndentedString(taiList)).append("\n");
    sb.append("    taiRangeList: ").append(toIndentedString(taiRangeList)).append("\n");
    sb.append("    pgwFqdn: ").append(toIndentedString(pgwFqdn)).append("\n");
    sb.append("    accessType: ").append(toIndentedString(accessType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

