package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.NFProfile;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SearchResult
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class SearchResult   {
  @JsonProperty("validityPeriod")
  private Integer validityPeriod;

  @JsonProperty("nfInstances")
  @Valid
  private List<NFProfile> nfInstances = new ArrayList<>();

  @JsonProperty("nrfSupportedFeatures")
  private String nrfSupportedFeatures;

  public SearchResult validityPeriod(Integer validityPeriod) {
    this.validityPeriod = validityPeriod;
    return this;
  }

  /**
   * Get validityPeriod
   * @return validityPeriod
  */
  @ApiModelProperty(value = "")


  public Integer getValidityPeriod() {
    return validityPeriod;
  }

  public void setValidityPeriod(Integer validityPeriod) {
    this.validityPeriod = validityPeriod;
  }

  public SearchResult nfInstances(List<NFProfile> nfInstances) {
    this.nfInstances = nfInstances;
    return this;
  }

  public SearchResult addNfInstancesItem(NFProfile nfInstancesItem) {
    this.nfInstances.add(nfInstancesItem);
    return this;
  }

  /**
   * Get nfInstances
   * @return nfInstances
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<NFProfile> getNfInstances() {
    return nfInstances;
  }

  public void setNfInstances(List<NFProfile> nfInstances) {
    this.nfInstances = nfInstances;
  }

  public SearchResult nrfSupportedFeatures(String nrfSupportedFeatures) {
    this.nrfSupportedFeatures = nrfSupportedFeatures;
    return this;
  }

  /**
   * Get nrfSupportedFeatures
   * @return nrfSupportedFeatures
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^[A-Fa-f0-9]*$") 
  public String getNrfSupportedFeatures() {
    return nrfSupportedFeatures;
  }

  public void setNrfSupportedFeatures(String nrfSupportedFeatures) {
    this.nrfSupportedFeatures = nrfSupportedFeatures;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SearchResult searchResult = (SearchResult) o;
    return Objects.equals(this.validityPeriod, searchResult.validityPeriod) &&
        Objects.equals(this.nfInstances, searchResult.nfInstances) &&
        Objects.equals(this.nrfSupportedFeatures, searchResult.nrfSupportedFeatures);
  }

  @Override
  public int hashCode() {
    return Objects.hash(validityPeriod, nfInstances, nrfSupportedFeatures);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SearchResult {\n");
    
    sb.append("    validityPeriod: ").append(toIndentedString(validityPeriod)).append("\n");
    sb.append("    nfInstances: ").append(toIndentedString(nfInstances)).append("\n");
    sb.append("    nrfSupportedFeatures: ").append(toIndentedString(nrfSupportedFeatures)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

