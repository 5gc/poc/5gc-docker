package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.N1MessageClass;
import com.t5g.nf.nrf.nfdiscovery.model.N2InformationClass;
import com.t5g.nf.nrf.nfdiscovery.model.NotificationType;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DefaultNotificationSubscription
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class DefaultNotificationSubscription   {
  @JsonProperty("notificationType")
  private NotificationType notificationType = null;

  @JsonProperty("callbackUri")
  private String callbackUri;

  @JsonProperty("n1MessageClass")
  private N1MessageClass n1MessageClass = null;

  @JsonProperty("n2InformationClass")
  private N2InformationClass n2InformationClass = null;

  public DefaultNotificationSubscription notificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
    return this;
  }

  /**
   * Get notificationType
   * @return notificationType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public NotificationType getNotificationType() {
    return notificationType;
  }

  public void setNotificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  public DefaultNotificationSubscription callbackUri(String callbackUri) {
    this.callbackUri = callbackUri;
    return this;
  }

  /**
   * Get callbackUri
   * @return callbackUri
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCallbackUri() {
    return callbackUri;
  }

  public void setCallbackUri(String callbackUri) {
    this.callbackUri = callbackUri;
  }

  public DefaultNotificationSubscription n1MessageClass(N1MessageClass n1MessageClass) {
    this.n1MessageClass = n1MessageClass;
    return this;
  }

  /**
   * Get n1MessageClass
   * @return n1MessageClass
  */
  @ApiModelProperty(value = "")

  @Valid

  public N1MessageClass getN1MessageClass() {
    return n1MessageClass;
  }

  public void setN1MessageClass(N1MessageClass n1MessageClass) {
    this.n1MessageClass = n1MessageClass;
  }

  public DefaultNotificationSubscription n2InformationClass(N2InformationClass n2InformationClass) {
    this.n2InformationClass = n2InformationClass;
    return this;
  }

  /**
   * Get n2InformationClass
   * @return n2InformationClass
  */
  @ApiModelProperty(value = "")

  @Valid

  public N2InformationClass getN2InformationClass() {
    return n2InformationClass;
  }

  public void setN2InformationClass(N2InformationClass n2InformationClass) {
    this.n2InformationClass = n2InformationClass;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DefaultNotificationSubscription defaultNotificationSubscription = (DefaultNotificationSubscription) o;
    return Objects.equals(this.notificationType, defaultNotificationSubscription.notificationType) &&
        Objects.equals(this.callbackUri, defaultNotificationSubscription.callbackUri) &&
        Objects.equals(this.n1MessageClass, defaultNotificationSubscription.n1MessageClass) &&
        Objects.equals(this.n2InformationClass, defaultNotificationSubscription.n2InformationClass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(notificationType, callbackUri, n1MessageClass, n2InformationClass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DefaultNotificationSubscription {\n");
    
    sb.append("    notificationType: ").append(toIndentedString(notificationType)).append("\n");
    sb.append("    callbackUri: ").append(toIndentedString(callbackUri)).append("\n");
    sb.append("    n1MessageClass: ").append(toIndentedString(n1MessageClass)).append("\n");
    sb.append("    n2InformationClass: ").append(toIndentedString(n2InformationClass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

