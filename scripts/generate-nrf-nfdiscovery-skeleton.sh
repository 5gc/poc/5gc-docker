#!/bin/sh

java -jar openapi-generator-cli.jar generate \
  -i TS29510_Nnrf_NFDiscovery.yaml \
  -g spring \
  --invoker-package com.t5g.nf.nrf.nfdiscovery \
  --api-package com.t5g.nf.nrf.nfdiscovery.api \
  --model-package com.t5g.nf.nrf.nfdiscovery.model \
  --group-id com.t5g.nf \
  --artifact-id nrf-nfdiscovery \
  --artifact-version 0.0.1-SNAPSHOT \
  -o nrf-nfdiscovery