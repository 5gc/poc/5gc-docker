package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ConfirmationData
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class ConfirmationData   {
  @JsonProperty("resStar")
  private String resStar;

  public ConfirmationData resStar(String resStar) {
    this.resStar = resStar;
    return this;
  }

  /**
   * Get resStar
   * @return resStar
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Fa-f0-9]{32}") 
  public String getResStar() {
    return resStar;
  }

  public void setResStar(String resStar) {
    this.resStar = resStar;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfirmationData confirmationData = (ConfirmationData) o;
    return Objects.equals(this.resStar, confirmationData.resStar);
  }

  @Override
  public int hashCode() {
    return Objects.hash(resStar);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfirmationData {\n");
    
    sb.append("    resStar: ").append(toIndentedString(resStar)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

