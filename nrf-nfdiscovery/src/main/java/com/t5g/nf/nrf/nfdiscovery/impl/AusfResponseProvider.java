package com.t5g.nf.nrf.nfdiscovery.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.t5g.nf.nrf.nfdiscovery.model.IpEndPoint;
import com.t5g.nf.nrf.nfdiscovery.model.NFProfile;
import com.t5g.nf.nrf.nfdiscovery.model.NFService;
import com.t5g.nf.nrf.nfdiscovery.model.SearchResult;

@Component
public class AusfResponseProvider {
	
	@Value("${stub.response.ausf.ip}")
	private String ausfIp;
	
	@Value("${stub.response.ausf.port}")
	private String ausfPort;
	
	@Value("${stub.response.ausf.fqdn}")
	private String ausfFqdn;

	
	public SearchResult getData() {
		IpEndPoint ipEndPoint = new IpEndPoint();
		ipEndPoint.setIpv4Address(ausfIp);
		ipEndPoint.setPort(Integer.valueOf(ausfPort));
		
		NFService nfService = new NFService();
		nfService.addIpEndPointsItem(ipEndPoint);
		
		NFProfile nfInstance = new NFProfile();
		nfInstance.addNfServicesItem(nfService);
		nfInstance.setFqdn(ausfFqdn);
		nfInstance.addIpv4AddressesItem(ausfIp);
		
		SearchResult res = new SearchResult();
		res.addNfInstancesItem(nfInstance);
		
		return res;
	}
	
}
