package com.t5g.nf.amf.emulator.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.t5g.nf.amf.emulator.model.AuthRequest;
import com.t5g.nf.amf.emulator.model.AuthResponse;
import com.t5g.nf.amf.emulator.util.Logger;
import com.t5g.nf.ausf.ueauthentication.model.AuthenticationInfo;
import com.t5g.nf.ausf.ueauthentication.model.ConfirmationData;
import com.t5g.nf.ausf.ueauthentication.model.ConfirmationDataResponse;
import com.t5g.nf.ausf.ueauthentication.model.UEAuthenticationCtx;
import com.t5g.nf.nrf.nfdiscovery.model.NFProfile;
import com.t5g.nf.nrf.nfdiscovery.model.SearchResult;

@Controller
@RequestMapping("subflow")
public class SubFlowController {
	
	private static final String SUPI_PATTERN = "^imsi-[0-9]{6,16}$";

	private static final String SERVING_NETWORK_NAME_TPLT = "5G:mnc%s.mcc%s.3gppnetwork.org";

	private static final HttpHeaders HEADERS;
	
	static {
        HEADERS = new HttpHeaders();
        HEADERS.setAccept(Collections.singletonList(MediaType.ALL));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

	@Autowired
	@Qualifier("NrfNfDiscoveryRestTemplate")
	private RestTemplate nrfRestTemplate;
	
	@Autowired
	@Qualifier("CommonRestTemplate")
	private RestTemplate commonRestTemplate;
	
	@Autowired
	@Qualifier("nrfNfDiscoveryUrl")
	private String nrfNfDiscoveryUrl;
	
	private boolean interceptorsOrdered;
	

	@PostMapping(path = "auth")
	public ResponseEntity<AuthResponse> invokeAuthenticationSubflow(@RequestBody AuthRequest req)
			throws RestClientException, URISyntaxException {
		
		final String supi = req.getSupi();
		
		if (!isValidSupi(supi)) {
			return ResponseEntity.badRequest()
					.body(new AuthResponse(String.format("Invalid SUPI: %s. Expected format: %s", supi, SUPI_PATTERN)));
		}
		
		orderInterceptorsOnFirstCall();
		
		// NRF NFDiscovery call
		URI uri = UriComponentsBuilder.
				fromUriString(nrfNfDiscoveryUrl + "/nnrf-disc/v1/nf-instances")
				.queryParam("target-nf-type", "AUSF")
				.queryParam("requester-nf-type", "AMF")
				.queryParam("supi", supi)
				.build().toUri();
		
		log("NRF NFDiscovery request:", uri.toString());
		
		ResponseEntity<SearchResult> nrfResp = nrfRestTemplate.getForEntity(uri, SearchResult.class);
		
		log("NRF NFDiscovery result:", nrfResp.getBody().toString());

		NFProfile ausfInst = nrfResp.getBody().getNfInstances().get(0);
		String ausfHost = ausfInst.getFqdn();
		Integer ausfPort = ausfInst.getNfServices().get(0).getIpEndPoints().get(0).getPort();
		String ausfUeAuthUrl = "http://" + ausfHost + ":" + ausfPort + "/nausf-auth/v1/ue-authentications";

		// AUSF UEAuthentication call
		final AuthenticationInfo authInfo = new AuthenticationInfo();
		authInfo.setSupiOrSuci(supi);
		authInfo.setServingNetworkName(toServingNetworkName(supi));
		
		log("AUSF UEAuthentication request:", authInfo.toString());
		
		ResponseEntity<UEAuthenticationCtx> authResp = commonRestTemplate.exchange(new URI(ausfUeAuthUrl),
				HttpMethod.POST, new HttpEntity<AuthenticationInfo>(authInfo, HEADERS), UEAuthenticationCtx.class);
		
		log("AUSF UEAuthentication result:", authResp.getBody().toString());
		
		// AUSF UEAuthentication acknowledgement call
		String authAckResource = authResp.getBody().getLinks().get("5g-aka").getHref();
		ConfirmationData ackRequest = new ConfirmationData().resStar("DF78AB09DF78AB09DF78AB09DF78AB09");
		
		log("AUSF UEAuthentication Acknowledgement request:", ackRequest.toString());
				
		ResponseEntity<ConfirmationDataResponse> authAckResp = commonRestTemplate.exchange(new URI(authAckResource),
				HttpMethod.PUT, new HttpEntity<ConfirmationData>(ackRequest, HEADERS), ConfirmationDataResponse.class);
		
		log("AUSF UEAuthentication Acknowledgement result:", authAckResp.toString());
		
		final AuthResponse resp = new AuthResponse();
		resp.setAuthenticationContext(authResp.getBody());
		resp.setAcknowledgement(authAckResp.getBody());
		
		return ResponseEntity.ok().body(resp);
	}
	
	private boolean isValidSupi(String supi) {
		return supi != null && supi.matches(SUPI_PATTERN);
	}
	
	private String toServingNetworkName(String supi) {
		return String.format(SERVING_NETWORK_NAME_TPLT, supi.substring(5, 8), supi.substring(8, 11));
	}
	
	private void log(String... messages) {
		Logger.log(messages);
	}
	
	private void orderInterceptorsOnFirstCall() {
		// A quick workaround to make Opentracing interceptor (added implicitly) to be the first one in the chain.
		// This allows logging the tracing HTTP headers
		if (interceptorsOrdered) {
			return;
		}

		Collections.reverse(nrfRestTemplate.getInterceptors());
		Collections.reverse(commonRestTemplate.getInterceptors());
		interceptorsOrdered = true;
	}

}
