package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.ausf.ueauthentication.model.ResynchronizationInfo;
import com.t5g.nf.ausf.ueauthentication.model.TraceData;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AuthenticationInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class AuthenticationInfo   {
  @JsonProperty("supiOrSuci")
  private String supiOrSuci;

  @JsonProperty("servingNetworkName")
  private String servingNetworkName;

  @JsonProperty("resynchronizationInfo")
  private ResynchronizationInfo resynchronizationInfo;

  @JsonProperty("pei")
  private String pei;

  @JsonProperty("traceData")
  private TraceData traceData;

  public AuthenticationInfo supiOrSuci(String supiOrSuci) {
    this.supiOrSuci = supiOrSuci;
    return this;
  }

  /**
   * Get supiOrSuci
   * @return supiOrSuci
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^(imsi-[0-9]{5,15}|nai-.+| suci-(0-[0-9]{3}-[0-9]{2,3}|[1-7]-.+)-[0-9]{1,4}-(0-0-.+|[a-fA-F1-9]-([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])-[a-fA-F0-9]+)|.+)$") 
  public String getSupiOrSuci() {
    return supiOrSuci;
  }

  public void setSupiOrSuci(String supiOrSuci) {
    this.supiOrSuci = supiOrSuci;
  }

  public AuthenticationInfo servingNetworkName(String servingNetworkName) {
    this.servingNetworkName = servingNetworkName;
    return this;
  }

  /**
   * Get servingNetworkName
   * @return servingNetworkName
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^5G:mnc[0-9]{3}[.]mcc[0-9]{3}[.]3gppnetwork[.]org$") 
  public String getServingNetworkName() {
    return servingNetworkName;
  }

  public void setServingNetworkName(String servingNetworkName) {
    this.servingNetworkName = servingNetworkName;
  }

  public AuthenticationInfo resynchronizationInfo(ResynchronizationInfo resynchronizationInfo) {
    this.resynchronizationInfo = resynchronizationInfo;
    return this;
  }

  /**
   * Get resynchronizationInfo
   * @return resynchronizationInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public ResynchronizationInfo getResynchronizationInfo() {
    return resynchronizationInfo;
  }

  public void setResynchronizationInfo(ResynchronizationInfo resynchronizationInfo) {
    this.resynchronizationInfo = resynchronizationInfo;
  }

  public AuthenticationInfo pei(String pei) {
    this.pei = pei;
    return this;
  }

  /**
   * Get pei
   * @return pei
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^(imei-[0-9]{15}|imeisv-[0-9]{16}|.+)$") 
  public String getPei() {
    return pei;
  }

  public void setPei(String pei) {
    this.pei = pei;
  }

  public AuthenticationInfo traceData(TraceData traceData) {
    this.traceData = traceData;
    return this;
  }

  /**
   * Get traceData
   * @return traceData
  */
  @ApiModelProperty(value = "")

  @Valid

  public TraceData getTraceData() {
    return traceData;
  }

  public void setTraceData(TraceData traceData) {
    this.traceData = traceData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthenticationInfo authenticationInfo = (AuthenticationInfo) o;
    return Objects.equals(this.supiOrSuci, authenticationInfo.supiOrSuci) &&
        Objects.equals(this.servingNetworkName, authenticationInfo.servingNetworkName) &&
        Objects.equals(this.resynchronizationInfo, authenticationInfo.resynchronizationInfo) &&
        Objects.equals(this.pei, authenticationInfo.pei) &&
        Objects.equals(this.traceData, authenticationInfo.traceData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supiOrSuci, servingNetworkName, resynchronizationInfo, pei, traceData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthenticationInfo {\n");
    
    sb.append("    supiOrSuci: ").append(toIndentedString(supiOrSuci)).append("\n");
    sb.append("    servingNetworkName: ").append(toIndentedString(servingNetworkName)).append("\n");
    sb.append("    resynchronizationInfo: ").append(toIndentedString(resynchronizationInfo)).append("\n");
    sb.append("    pei: ").append(toIndentedString(pei)).append("\n");
    sb.append("    traceData: ").append(toIndentedString(traceData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

