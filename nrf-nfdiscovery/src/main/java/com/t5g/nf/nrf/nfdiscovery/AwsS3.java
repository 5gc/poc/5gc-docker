package com.t5g.nf.nrf.nfdiscovery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;

//TODO: Complete the configuration loading from AWS S3
@Component
public class AwsS3 {
	
	@Value("${config.aws.enabled}")
	boolean configAwsEnabled;

	
	@PostConstruct
	public void postConstruct() {
		if (!configAwsEnabled) {
			return;
		}
		
		try (S3Client s3Client = S3Client.builder().region(Region.EU_CENTRAL_1).build()) {
			
			final ResponseInputStream<GetObjectResponse> bucketObject = s3Client.getObject(
					GetObjectRequest.builder().bucket("t5g.emulator.config").key("test.json").build(),
					ResponseTransformer.toInputStream());
			
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(bucketObject))) {

				String contents = reader.lines().collect(Collectors.joining("\n"));
				System.out.println(contents);

			} catch (AwsServiceException | SdkClientException | IOException e) {
				throw new IllegalStateException("Configuration loading failure", e);
			}

		}
	}

}
