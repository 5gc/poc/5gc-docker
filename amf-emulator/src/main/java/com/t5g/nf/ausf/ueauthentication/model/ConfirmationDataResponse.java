package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.ausf.ueauthentication.model.AuthResult;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ConfirmationDataResponse
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class ConfirmationDataResponse   {
  @JsonProperty("authResult")
  private AuthResult authResult;

  @JsonProperty("supi")
  private String supi;

  @JsonProperty("kseaf")
  private String kseaf;

  public ConfirmationDataResponse authResult(AuthResult authResult) {
    this.authResult = authResult;
    return this;
  }

  /**
   * Get authResult
   * @return authResult
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthResult getAuthResult() {
    return authResult;
  }

  public void setAuthResult(AuthResult authResult) {
    this.authResult = authResult;
  }

  public ConfirmationDataResponse supi(String supi) {
    this.supi = supi;
    return this;
  }

  /**
   * Get supi
   * @return supi
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^(imsi-[0-9]{5,15}|nai-.+|.+)$") 
  public String getSupi() {
    return supi;
  }

  public void setSupi(String supi) {
    this.supi = supi;
  }

  public ConfirmationDataResponse kseaf(String kseaf) {
    this.kseaf = kseaf;
    return this;
  }

  /**
   * Get kseaf
   * @return kseaf
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Fa-f0-9]{64}") 
  public String getKseaf() {
    return kseaf;
  }

  public void setKseaf(String kseaf) {
    this.kseaf = kseaf;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfirmationDataResponse confirmationDataResponse = (ConfirmationDataResponse) o;
    return Objects.equals(this.authResult, confirmationDataResponse.authResult) &&
        Objects.equals(this.supi, confirmationDataResponse.supi) &&
        Objects.equals(this.kseaf, confirmationDataResponse.kseaf);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authResult, supi, kseaf);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfirmationDataResponse {\n");
    
    sb.append("    authResult: ").append(toIndentedString(authResult)).append("\n");
    sb.append("    supi: ").append(toIndentedString(supi)).append("\n");
    sb.append("    kseaf: ").append(toIndentedString(kseaf)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

