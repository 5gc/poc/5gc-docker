package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Av5gAka
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class Av5gAka   {
  @JsonProperty("rand")
  private String rand;

  @JsonProperty("hxresStar")
  private String hxresStar;

  @JsonProperty("autn")
  private String autn;

  public Av5gAka rand(String rand) {
    this.rand = rand;
    return this;
  }

  /**
   * Get rand
   * @return rand
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]{32}$") 
  public String getRand() {
    return rand;
  }

  public void setRand(String rand) {
    this.rand = rand;
  }

  public Av5gAka hxresStar(String hxresStar) {
    this.hxresStar = hxresStar;
    return this;
  }

  /**
   * Get hxresStar
   * @return hxresStar
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Fa-f0-9]{32}") 
  public String getHxresStar() {
    return hxresStar;
  }

  public void setHxresStar(String hxresStar) {
    this.hxresStar = hxresStar;
  }

  public Av5gAka autn(String autn) {
    this.autn = autn;
    return this;
  }

  /**
   * Get autn
   * @return autn
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]{32}$") 
  public String getAutn() {
    return autn;
  }

  public void setAutn(String autn) {
    this.autn = autn;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Av5gAka av5gAka = (Av5gAka) o;
    return Objects.equals(this.rand, av5gAka.rand) &&
        Objects.equals(this.hxresStar, av5gAka.hxresStar) &&
        Objects.equals(this.autn, av5gAka.autn);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rand, hxresStar, autn);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Av5gAka {\n");
    
    sb.append("    rand: ").append(toIndentedString(rand)).append("\n");
    sb.append("    hxresStar: ").append(toIndentedString(hxresStar)).append("\n");
    sb.append("    autn: ").append(toIndentedString(autn)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

