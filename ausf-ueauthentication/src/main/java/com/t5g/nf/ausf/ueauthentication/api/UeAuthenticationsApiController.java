package com.t5g.nf.ausf.ueauthentication.api;

import java.net.URI;
import java.util.HashMap;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import com.t5g.nf.ausf.ueauthentication.impl.AuthenticationCtxProvider;
import com.t5g.nf.ausf.ueauthentication.model.AuthResult;
import com.t5g.nf.ausf.ueauthentication.model.AuthenticationInfo;
import com.t5g.nf.ausf.ueauthentication.model.ConfirmationData;
import com.t5g.nf.ausf.ueauthentication.model.ConfirmationDataResponse;
import com.t5g.nf.ausf.ueauthentication.model.UEAuthenticationCtx;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-17T16:19:50.870+02:00[Europe/Helsinki]")

@Controller
@RequestMapping("${openapi.aUSF.base-path:/nausf-auth/v1}")
public class UeAuthenticationsApiController implements UeAuthenticationsApi {

	private final NativeWebRequest request;

	@Autowired
	private AuthenticationCtxProvider authCtxProvider;


	@org.springframework.beans.factory.annotation.Autowired
	public UeAuthenticationsApiController(NativeWebRequest request) {
		this.request = request;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<UEAuthenticationCtx> ueAuthenticationsPost(
			@ApiParam(value = "", required = true) @Valid @RequestBody AuthenticationInfo authenticationInfo) {

		final HashMap<String, String> headers = new HashMap<>();
		
		UEAuthenticationCtx authCtx = authCtxProvider.getContext(authenticationInfo, headers);
		authCtx.setServingNetworkName(authenticationInfo.getServingNetworkName());

		return ResponseEntity.created(URI.create(headers.get("Location"))).body(authCtx);

	}
	
	@Override
	public ResponseEntity<ConfirmationDataResponse> ueAuthenticationsAuthCtxId5gAkaConfirmationPut(
			@ApiParam(value = "", required = true) @PathVariable("authCtxId") String authCtxId,
			@ApiParam(value = "") @Valid @RequestBody ConfirmationData confirmationData) {
		
		ConfirmationDataResponse respData = new ConfirmationDataResponse();
		final String supi = authCtxProvider.getSupi(authCtxId);
		if (supi != null) {
			respData.supi(supi).authResult(AuthResult.SUCCESS);
		} else {
			respData.authResult(AuthResult.FAILURE);
		}
		
		return ResponseEntity.ok().body(respData);
	}

}
