package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.Atom;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CnfUnit
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class CnfUnit   {
  @JsonProperty("cnfUnit")
  @Valid
  private List<Atom> cnfUnit = new ArrayList<>();

  public CnfUnit cnfUnit(List<Atom> cnfUnit) {
    this.cnfUnit = cnfUnit;
    return this;
  }

  public CnfUnit addCnfUnitItem(Atom cnfUnitItem) {
    this.cnfUnit.add(cnfUnitItem);
    return this;
  }

  /**
   * Get cnfUnit
   * @return cnfUnit
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<Atom> getCnfUnit() {
    return cnfUnit;
  }

  public void setCnfUnit(List<Atom> cnfUnit) {
    this.cnfUnit = cnfUnit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CnfUnit cnfUnit = (CnfUnit) o;
    return Objects.equals(this.cnfUnit, cnfUnit.cnfUnit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cnfUnit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CnfUnit {\n");
    
    sb.append("    cnfUnit: ").append(toIndentedString(cnfUnit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

