package com.t5g.nf.amf.emulator;

import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.jaegertracing.Configuration.ReporterConfiguration;
import io.jaegertracing.Configuration.SamplerConfiguration;
import io.jaegertracing.Configuration.SenderConfiguration;
import io.jaegertracing.internal.samplers.ConstSampler;
import io.opentracing.Tracer;

@Configuration
public class AppConfig {

	private static final int TIMEOUT_MILLIS = 60000;

	@Value("${nrf.nfdiscovery.host}")
	private String nrfNfdiscoveryHost;

	@Value("${nrf.nfdiscovery.port}")
	private String nrfNfdiscoveryPort;

	@Value("${nrf.protocol}")
	private String nrfProtocol;

	@Value("${jaeger.agent.host}")
	private String jaegerAgentHost;

	@Value("${jaeger.agent.port}")
	private String jaegerAgentPort;

	@Bean
	public JsonNullableModule jsonNullableModule() {
		return new JsonNullableModule();
	}

	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.serializationInclusion(JsonInclude.Include.NON_NULL);
		builder.modulesToInstall(jsonNullableModule());

		return builder;
	}

	@Bean(name = "nrfNfDiscoveryUrl")
	public String nrfNfDiscoveryUrl() {
		return nrfProtocol + "://" + nrfNfdiscoveryHost + ":" + nrfNfdiscoveryPort;
	}
	
	@Bean
	public ClientHttpRequestFactory clientHttpRequestFactory() {
		return new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
	}

	@Bean
	public ClientHttpRequestInterceptor httpLoggingInterceptor() {
		return new HttpLoggingInterceptor();
	}

	@Bean
	public RestTemplateBuilder nrfNfDiscoveryRestTemplateBuilder() {
		return new RestTemplateBuilder().rootUri(nrfNfDiscoveryUrl()).setReadTimeout(TIMEOUT_MILLIS);
	}

	@Bean
	public RestTemplateBuilder commonRestTemplateBuilder() {
		return new RestTemplateBuilder().setReadTimeout(TIMEOUT_MILLIS);
	}

	@Bean(name = "NrfNfDiscoveryRestTemplate")
	@DependsOn({"Tracer"})
	public RestTemplate nrfNfDiscoveryRestTemplate() {
		final RestTemplate restTemplate = nrfNfDiscoveryRestTemplateBuilder().build();
		restTemplate.setRequestFactory(clientHttpRequestFactory());
		restTemplate.getInterceptors().add(httpLoggingInterceptor());

		return restTemplate;
	}

	@Bean(name = "CommonRestTemplate")
	@DependsOn({"Tracer"})
	public RestTemplate commonRestTemplate() {
		final RestTemplate restTemplate = commonRestTemplateBuilder().build();
		restTemplate.setRequestFactory(clientHttpRequestFactory());
		restTemplate.getInterceptors().add(httpLoggingInterceptor());
		return restTemplate;
	}

	@Bean(name = "Tracer")
	public Tracer jaegerTracer() {
		io.jaegertracing.Configuration config = new io.jaegertracing.Configuration("AMF Emulator");

		return config.withSampler(new SamplerConfiguration().withType(ConstSampler.TYPE).withParam(1))
				.withReporter(new ReporterConfiguration().withSender(new SenderConfiguration()
						.withAgentHost(jaegerAgentHost).withAgentPort(Integer.valueOf(jaegerAgentPort))))
				.getTracer();

	}

}
