package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.ausf.ueauthentication.model.AuthResult;
import com.t5g.nf.ausf.ueauthentication.model.LinksValueSchema;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * EapSession
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class EapSession   {
  @JsonProperty("eapPayload")
  private String eapPayload;

  @JsonProperty("kSeaf")
  private String kSeaf;

  @JsonProperty("_links")
  @Valid
  private Map<String, LinksValueSchema> links = null;

  @JsonProperty("authResult")
  private AuthResult authResult;

  @JsonProperty("supi")
  private String supi;

  public EapSession eapPayload(String eapPayload) {
    this.eapPayload = eapPayload;
    return this;
  }

  /**
   * contains an EAP packet
   * @return eapPayload
  */
  @ApiModelProperty(required = true, value = "contains an EAP packet")
  @NotNull


  public String getEapPayload() {
    return eapPayload;
  }

  public void setEapPayload(String eapPayload) {
    this.eapPayload = eapPayload;
  }

  public EapSession kSeaf(String kSeaf) {
    this.kSeaf = kSeaf;
    return this;
  }

  /**
   * Get kSeaf
   * @return kSeaf
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Fa-f0-9]{64}") 
  public String getkSeaf() {
    return kSeaf;
  }

  public void setkSeaf(String kSeaf) {
    this.kSeaf = kSeaf;
  }

  public EapSession links(Map<String, LinksValueSchema> links) {
    this.links = links;
    return this;
  }

  public EapSession putLinksItem(String key, LinksValueSchema linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @ApiModelProperty(value = "")

  @Valid

  public Map<String, LinksValueSchema> getLinks() {
    return links;
  }

  public void setLinks(Map<String, LinksValueSchema> links) {
    this.links = links;
  }

  public EapSession authResult(AuthResult authResult) {
    this.authResult = authResult;
    return this;
  }

  /**
   * Get authResult
   * @return authResult
  */
  @ApiModelProperty(value = "")

  @Valid

  public AuthResult getAuthResult() {
    return authResult;
  }

  public void setAuthResult(AuthResult authResult) {
    this.authResult = authResult;
  }

  public EapSession supi(String supi) {
    this.supi = supi;
    return this;
  }

  /**
   * Get supi
   * @return supi
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^(imsi-[0-9]{5,15}|nai-.+|.+)$") 
  public String getSupi() {
    return supi;
  }

  public void setSupi(String supi) {
    this.supi = supi;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EapSession eapSession = (EapSession) o;
    return Objects.equals(this.eapPayload, eapSession.eapPayload) &&
        Objects.equals(this.kSeaf, eapSession.kSeaf) &&
        Objects.equals(this.links, eapSession.links) &&
        Objects.equals(this.authResult, eapSession.authResult) &&
        Objects.equals(this.supi, eapSession.supi);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eapPayload, kSeaf, links, authResult, supi);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EapSession {\n");
    
    sb.append("    eapPayload: ").append(toIndentedString(eapPayload)).append("\n");
    sb.append("    kSeaf: ").append(toIndentedString(kSeaf)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    authResult: ").append(toIndentedString(authResult)).append("\n");
    sb.append("    supi: ").append(toIndentedString(supi)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

