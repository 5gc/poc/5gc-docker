package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.InterfaceUpfInfoItem;
import com.t5g.nf.nrf.nfdiscovery.model.PduSessionType;
import com.t5g.nf.nrf.nfdiscovery.model.SnssaiUpfInfoItem;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UpfInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class UpfInfo   {
  @JsonProperty("sNssaiUpfInfoList")
  @Valid
  private List<SnssaiUpfInfoItem> sNssaiUpfInfoList = new ArrayList<>();

  @JsonProperty("smfServingArea")
  @Valid
  private List<String> smfServingArea = null;

  @JsonProperty("interfaceUpfInfoList")
  @Valid
  private List<InterfaceUpfInfoItem> interfaceUpfInfoList = null;

  @JsonProperty("iwkEpsInd")
  private Boolean iwkEpsInd = false;

  @JsonProperty("pduSessionTypes")
  @Valid
  private List<PduSessionType> pduSessionTypes = null;

  public UpfInfo sNssaiUpfInfoList(List<SnssaiUpfInfoItem> sNssaiUpfInfoList) {
    this.sNssaiUpfInfoList = sNssaiUpfInfoList;
    return this;
  }

  public UpfInfo addSNssaiUpfInfoListItem(SnssaiUpfInfoItem sNssaiUpfInfoListItem) {
    this.sNssaiUpfInfoList.add(sNssaiUpfInfoListItem);
    return this;
  }

  /**
   * Get sNssaiUpfInfoList
   * @return sNssaiUpfInfoList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<SnssaiUpfInfoItem> getsNssaiUpfInfoList() {
    return sNssaiUpfInfoList;
  }

  public void setsNssaiUpfInfoList(List<SnssaiUpfInfoItem> sNssaiUpfInfoList) {
    this.sNssaiUpfInfoList = sNssaiUpfInfoList;
  }

  public UpfInfo smfServingArea(List<String> smfServingArea) {
    this.smfServingArea = smfServingArea;
    return this;
  }

  public UpfInfo addSmfServingAreaItem(String smfServingAreaItem) {
    if (this.smfServingArea == null) {
      this.smfServingArea = new ArrayList<>();
    }
    this.smfServingArea.add(smfServingAreaItem);
    return this;
  }

  /**
   * Get smfServingArea
   * @return smfServingArea
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getSmfServingArea() {
    return smfServingArea;
  }

  public void setSmfServingArea(List<String> smfServingArea) {
    this.smfServingArea = smfServingArea;
  }

  public UpfInfo interfaceUpfInfoList(List<InterfaceUpfInfoItem> interfaceUpfInfoList) {
    this.interfaceUpfInfoList = interfaceUpfInfoList;
    return this;
  }

  public UpfInfo addInterfaceUpfInfoListItem(InterfaceUpfInfoItem interfaceUpfInfoListItem) {
    if (this.interfaceUpfInfoList == null) {
      this.interfaceUpfInfoList = new ArrayList<>();
    }
    this.interfaceUpfInfoList.add(interfaceUpfInfoListItem);
    return this;
  }

  /**
   * Get interfaceUpfInfoList
   * @return interfaceUpfInfoList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<InterfaceUpfInfoItem> getInterfaceUpfInfoList() {
    return interfaceUpfInfoList;
  }

  public void setInterfaceUpfInfoList(List<InterfaceUpfInfoItem> interfaceUpfInfoList) {
    this.interfaceUpfInfoList = interfaceUpfInfoList;
  }

  public UpfInfo iwkEpsInd(Boolean iwkEpsInd) {
    this.iwkEpsInd = iwkEpsInd;
    return this;
  }

  /**
   * Get iwkEpsInd
   * @return iwkEpsInd
  */
  @ApiModelProperty(value = "")


  public Boolean getIwkEpsInd() {
    return iwkEpsInd;
  }

  public void setIwkEpsInd(Boolean iwkEpsInd) {
    this.iwkEpsInd = iwkEpsInd;
  }

  public UpfInfo pduSessionTypes(List<PduSessionType> pduSessionTypes) {
    this.pduSessionTypes = pduSessionTypes;
    return this;
  }

  public UpfInfo addPduSessionTypesItem(PduSessionType pduSessionTypesItem) {
    if (this.pduSessionTypes == null) {
      this.pduSessionTypes = new ArrayList<>();
    }
    this.pduSessionTypes.add(pduSessionTypesItem);
    return this;
  }

  /**
   * Get pduSessionTypes
   * @return pduSessionTypes
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<PduSessionType> getPduSessionTypes() {
    return pduSessionTypes;
  }

  public void setPduSessionTypes(List<PduSessionType> pduSessionTypes) {
    this.pduSessionTypes = pduSessionTypes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpfInfo upfInfo = (UpfInfo) o;
    return Objects.equals(this.sNssaiUpfInfoList, upfInfo.sNssaiUpfInfoList) &&
        Objects.equals(this.smfServingArea, upfInfo.smfServingArea) &&
        Objects.equals(this.interfaceUpfInfoList, upfInfo.interfaceUpfInfoList) &&
        Objects.equals(this.iwkEpsInd, upfInfo.iwkEpsInd) &&
        Objects.equals(this.pduSessionTypes, upfInfo.pduSessionTypes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sNssaiUpfInfoList, smfServingArea, interfaceUpfInfoList, iwkEpsInd, pduSessionTypes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpfInfo {\n");
    
    sb.append("    sNssaiUpfInfoList: ").append(toIndentedString(sNssaiUpfInfoList)).append("\n");
    sb.append("    smfServingArea: ").append(toIndentedString(smfServingArea)).append("\n");
    sb.append("    interfaceUpfInfoList: ").append(toIndentedString(interfaceUpfInfoList)).append("\n");
    sb.append("    iwkEpsInd: ").append(toIndentedString(iwkEpsInd)).append("\n");
    sb.append("    pduSessionTypes: ").append(toIndentedString(pduSessionTypes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

