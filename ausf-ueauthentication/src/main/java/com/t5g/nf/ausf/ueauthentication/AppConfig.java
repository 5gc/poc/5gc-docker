package com.t5g.nf.ausf.ueauthentication;

import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.Module;

import io.jaegertracing.Configuration.ReporterConfiguration;
import io.jaegertracing.Configuration.SamplerConfiguration;
import io.jaegertracing.Configuration.SenderConfiguration;
import io.jaegertracing.internal.samplers.ConstSampler;
import io.opentracing.Tracer;

@Configuration
public class AppConfig {

	@Value("${jaeger.agent.host}")
	private String jaegerAgentHost;

	@Value("${jaeger.agent.port}")
	private String jaegerAgentPort;
	
	
	@Bean
	public JsonNullableModule jsonNullableModule() {
		return new JsonNullableModule();
	}
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.serializationInclusion(JsonInclude.Include.NON_NULL);
		builder.modulesToInstall(jsonNullableModule());

		return builder;
	}

	@Bean
	public Tracer jaegerTracer() {
		io.jaegertracing.Configuration config = new io.jaegertracing.Configuration("AUSF UEAuthentication");
		
		return config.withSampler(new SamplerConfiguration().withType(ConstSampler.TYPE).withParam(1))
				.withReporter(new ReporterConfiguration().withSender(new SenderConfiguration()
						.withAgentHost(jaegerAgentHost).withAgentPort(Integer.valueOf(jaegerAgentPort))))
				.getTracer();

	}

}
