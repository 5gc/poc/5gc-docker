package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.ausf.ueauthentication.model.TraceDepth;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TraceData
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class TraceData   {
  @JsonProperty("traceRef")
  private String traceRef;

  @JsonProperty("traceDepth")
  private TraceDepth traceDepth = null;

  @JsonProperty("neTypeList")
  private String neTypeList;

  @JsonProperty("eventList")
  private String eventList;

  @JsonProperty("collectionEntityIpv4Addr")
  private String collectionEntityIpv4Addr;

  @JsonProperty("collectionEntityIpv6Addr")
  private String collectionEntityIpv6Addr;

  @JsonProperty("interfaceList")
  private String interfaceList;

  public TraceData traceRef(String traceRef) {
    this.traceRef = traceRef;
    return this;
  }

  /**
   * Get traceRef
   * @return traceRef
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[0-9]{3}[0-9]{2,3}-[A-Fa-f0-9]{6}$") 
  public String getTraceRef() {
    return traceRef;
  }

  public void setTraceRef(String traceRef) {
    this.traceRef = traceRef;
  }

  public TraceData traceDepth(TraceDepth traceDepth) {
    this.traceDepth = traceDepth;
    return this;
  }

  /**
   * Get traceDepth
   * @return traceDepth
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public TraceDepth getTraceDepth() {
    return traceDepth;
  }

  public void setTraceDepth(TraceDepth traceDepth) {
    this.traceDepth = traceDepth;
  }

  public TraceData neTypeList(String neTypeList) {
    this.neTypeList = neTypeList;
    return this;
  }

  /**
   * Get neTypeList
   * @return neTypeList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]+$") 
  public String getNeTypeList() {
    return neTypeList;
  }

  public void setNeTypeList(String neTypeList) {
    this.neTypeList = neTypeList;
  }

  public TraceData eventList(String eventList) {
    this.eventList = eventList;
    return this;
  }

  /**
   * Get eventList
   * @return eventList
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]+$") 
  public String getEventList() {
    return eventList;
  }

  public void setEventList(String eventList) {
    this.eventList = eventList;
  }

  public TraceData collectionEntityIpv4Addr(String collectionEntityIpv4Addr) {
    this.collectionEntityIpv4Addr = collectionEntityIpv4Addr;
    return this;
  }

  /**
   * Get collectionEntityIpv4Addr
   * @return collectionEntityIpv4Addr
  */
  @ApiModelProperty(example = "198.51.100.1", value = "")

@Pattern(regexp="^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$") 
  public String getCollectionEntityIpv4Addr() {
    return collectionEntityIpv4Addr;
  }

  public void setCollectionEntityIpv4Addr(String collectionEntityIpv4Addr) {
    this.collectionEntityIpv4Addr = collectionEntityIpv4Addr;
  }

  public TraceData collectionEntityIpv6Addr(String collectionEntityIpv6Addr) {
    this.collectionEntityIpv6Addr = collectionEntityIpv6Addr;
    return this;
  }

  /**
   * Get collectionEntityIpv6Addr
   * @return collectionEntityIpv6Addr
  */
  @ApiModelProperty(value = "")


  public String getCollectionEntityIpv6Addr() {
    return collectionEntityIpv6Addr;
  }

  public void setCollectionEntityIpv6Addr(String collectionEntityIpv6Addr) {
    this.collectionEntityIpv6Addr = collectionEntityIpv6Addr;
  }

  public TraceData interfaceList(String interfaceList) {
    this.interfaceList = interfaceList;
    return this;
  }

  /**
   * Get interfaceList
   * @return interfaceList
  */
  @ApiModelProperty(value = "")

@Pattern(regexp="^[A-Fa-f0-9]+$") 
  public String getInterfaceList() {
    return interfaceList;
  }

  public void setInterfaceList(String interfaceList) {
    this.interfaceList = interfaceList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TraceData traceData = (TraceData) o;
    return Objects.equals(this.traceRef, traceData.traceRef) &&
        Objects.equals(this.traceDepth, traceData.traceDepth) &&
        Objects.equals(this.neTypeList, traceData.neTypeList) &&
        Objects.equals(this.eventList, traceData.eventList) &&
        Objects.equals(this.collectionEntityIpv4Addr, traceData.collectionEntityIpv4Addr) &&
        Objects.equals(this.collectionEntityIpv6Addr, traceData.collectionEntityIpv6Addr) &&
        Objects.equals(this.interfaceList, traceData.interfaceList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(traceRef, traceDepth, neTypeList, eventList, collectionEntityIpv4Addr, collectionEntityIpv6Addr, interfaceList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TraceData {\n");
    
    sb.append("    traceRef: ").append(toIndentedString(traceRef)).append("\n");
    sb.append("    traceDepth: ").append(toIndentedString(traceDepth)).append("\n");
    sb.append("    neTypeList: ").append(toIndentedString(neTypeList)).append("\n");
    sb.append("    eventList: ").append(toIndentedString(eventList)).append("\n");
    sb.append("    collectionEntityIpv4Addr: ").append(toIndentedString(collectionEntityIpv4Addr)).append("\n");
    sb.append("    collectionEntityIpv6Addr: ").append(toIndentedString(collectionEntityIpv6Addr)).append("\n");
    sb.append("    interfaceList: ").append(toIndentedString(interfaceList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

