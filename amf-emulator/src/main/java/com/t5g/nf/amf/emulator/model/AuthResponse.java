package com.t5g.nf.amf.emulator.model;

import com.t5g.nf.ausf.ueauthentication.model.ConfirmationDataResponse;
import com.t5g.nf.ausf.ueauthentication.model.UEAuthenticationCtx;

public class AuthResponse {
	
	private String message;
	
	private UEAuthenticationCtx authenticationContext;
	
	private ConfirmationDataResponse acknowledgement; 

	
	public AuthResponse() {
	}
	
	public AuthResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String details) {
		this.message = details;
	}
	
	public UEAuthenticationCtx getAuthenticationContext() {
		return authenticationContext;
	}
	
	public void setAuthenticationContext(UEAuthenticationCtx authenticationContext) {
		this.authenticationContext = authenticationContext;
	}
	
	public ConfirmationDataResponse getAcknowledgement() {
		return acknowledgement;
	}
	
	public void setAcknowledgement(ConfirmationDataResponse acknowledgement) {
		this.acknowledgement = acknowledgement;
	}
	
}
