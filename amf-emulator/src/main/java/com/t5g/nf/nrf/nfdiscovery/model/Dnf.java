package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.DnfUnit;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Dnf
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class Dnf   {
  @JsonProperty("dnfUnits")
  @Valid
  private List<DnfUnit> dnfUnits = new ArrayList<>();

  public Dnf dnfUnits(List<DnfUnit> dnfUnits) {
    this.dnfUnits = dnfUnits;
    return this;
  }

  public Dnf addDnfUnitsItem(DnfUnit dnfUnitsItem) {
    this.dnfUnits.add(dnfUnitsItem);
    return this;
  }

  /**
   * Get dnfUnits
   * @return dnfUnits
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<DnfUnit> getDnfUnits() {
    return dnfUnits;
  }

  public void setDnfUnits(List<DnfUnit> dnfUnits) {
    this.dnfUnits = dnfUnits;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dnf dnf = (Dnf) o;
    return Objects.equals(this.dnfUnits, dnf.dnfUnits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dnfUnits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dnf {\n");
    
    sb.append("    dnfUnits: ").append(toIndentedString(dnfUnits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

