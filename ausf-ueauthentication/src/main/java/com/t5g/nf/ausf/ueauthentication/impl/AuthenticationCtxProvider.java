package com.t5g.nf.ausf.ueauthentication.impl;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.t5g.nf.ausf.ueauthentication.model.AuthType;
import com.t5g.nf.ausf.ueauthentication.model.AuthenticationInfo;
import com.t5g.nf.ausf.ueauthentication.model.Av5gAka;
import com.t5g.nf.ausf.ueauthentication.model.LinksValueSchema;
import com.t5g.nf.ausf.ueauthentication.model.UEAuthenticationCtx;
import com.t5g.nf.ausf.ueauthentication.util.NetworkUtils;

@Component
public class AuthenticationCtxProvider {

	@Autowired
	private NetworkUtils networkUtils;

	private Set<String> ctxIds = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	
	private Map<String, String> authCtxs = new ConcurrentHashMap<String, String>();

	public UEAuthenticationCtx getContext(AuthenticationInfo authInfo, Map<String, String> headers) {
		final UEAuthenticationCtx authCtx = new UEAuthenticationCtx();
		authCtx.setAuthType(AuthType._5G_AKA);

		final Av5gAka authVector = new Av5gAka();
		authVector.setRand("ABCD0123ABCD0123ABCD0123ABCD0123");
		authVector.setHxresStar("FACE5533FACE5533FACE5533FACE5533");
		authVector.setAutn("AAFF7777AAFF7777AAFF7777AAFF7777");
		authCtx.set5gAuthData(authVector);

		final String ctxId = generateContextId(authInfo.getSupiOrSuci());

		String ctxResLocation;
		try {
			ctxResLocation = networkUtils.getRootUrl() + "/nausf-auth/v1/ue-authentications/" + ctxId;
		} catch (UnknownHostException e) {
			throw new IllegalStateException("AUSF internal error: root URL determination failure", e);
		}

		headers.put("Location", ctxResLocation);

		authCtx.putLinksItem("5g-aka",
				(LinksValueSchema) new LinksValueSchema().href(ctxResLocation + "/5g-aka-confirmation"));

		return authCtx;
	}

	// Note, this is non-idempotent. The function call removes this id from the internal cache (if exists).
	// All consequent calls will always return false. This is done for PoC purposes only to emulate authentication
	// acknowledgement.
//	public boolean contextIdExists(String id) {
//		return ctxIds.remove(id);
//	}
	
	public String getSupi(String ctxId) {
		return authCtxs.get(ctxId);
	}

	private String generateContextId() {
		final String id = UUID.randomUUID().toString();
		ctxIds.add(id);

		return id;
	}
	
	private String generateContextId(String supi) {
		final String id = UUID.randomUUID().toString();
		authCtxs.put(id, supi);

		return id;
	}

}
