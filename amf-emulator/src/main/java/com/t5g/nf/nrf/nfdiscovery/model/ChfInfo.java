package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.IdentityRange;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnRange;
import com.t5g.nf.nrf.nfdiscovery.model.SupiRange;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChfInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class ChfInfo   {
  @JsonProperty("supiRangeList")
  @Valid
  private List<SupiRange> supiRangeList = null;

  @JsonProperty("gpsiRangeList")
  @Valid
  private List<IdentityRange> gpsiRangeList = null;

  @JsonProperty("plmnRangeList")
  @Valid
  private List<PlmnRange> plmnRangeList = null;

  public ChfInfo supiRangeList(List<SupiRange> supiRangeList) {
    this.supiRangeList = supiRangeList;
    return this;
  }

  public ChfInfo addSupiRangeListItem(SupiRange supiRangeListItem) {
    if (this.supiRangeList == null) {
      this.supiRangeList = new ArrayList<>();
    }
    this.supiRangeList.add(supiRangeListItem);
    return this;
  }

  /**
   * Get supiRangeList
   * @return supiRangeList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<SupiRange> getSupiRangeList() {
    return supiRangeList;
  }

  public void setSupiRangeList(List<SupiRange> supiRangeList) {
    this.supiRangeList = supiRangeList;
  }

  public ChfInfo gpsiRangeList(List<IdentityRange> gpsiRangeList) {
    this.gpsiRangeList = gpsiRangeList;
    return this;
  }

  public ChfInfo addGpsiRangeListItem(IdentityRange gpsiRangeListItem) {
    if (this.gpsiRangeList == null) {
      this.gpsiRangeList = new ArrayList<>();
    }
    this.gpsiRangeList.add(gpsiRangeListItem);
    return this;
  }

  /**
   * Get gpsiRangeList
   * @return gpsiRangeList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<IdentityRange> getGpsiRangeList() {
    return gpsiRangeList;
  }

  public void setGpsiRangeList(List<IdentityRange> gpsiRangeList) {
    this.gpsiRangeList = gpsiRangeList;
  }

  public ChfInfo plmnRangeList(List<PlmnRange> plmnRangeList) {
    this.plmnRangeList = plmnRangeList;
    return this;
  }

  public ChfInfo addPlmnRangeListItem(PlmnRange plmnRangeListItem) {
    if (this.plmnRangeList == null) {
      this.plmnRangeList = new ArrayList<>();
    }
    this.plmnRangeList.add(plmnRangeListItem);
    return this;
  }

  /**
   * Get plmnRangeList
   * @return plmnRangeList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<PlmnRange> getPlmnRangeList() {
    return plmnRangeList;
  }

  public void setPlmnRangeList(List<PlmnRange> plmnRangeList) {
    this.plmnRangeList = plmnRangeList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChfInfo chfInfo = (ChfInfo) o;
    return Objects.equals(this.supiRangeList, chfInfo.supiRangeList) &&
        Objects.equals(this.gpsiRangeList, chfInfo.gpsiRangeList) &&
        Objects.equals(this.plmnRangeList, chfInfo.plmnRangeList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supiRangeList, gpsiRangeList, plmnRangeList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChfInfo {\n");
    
    sb.append("    supiRangeList: ").append(toIndentedString(supiRangeList)).append("\n");
    sb.append("    gpsiRangeList: ").append(toIndentedString(gpsiRangeList)).append("\n");
    sb.append("    plmnRangeList: ").append(toIndentedString(plmnRangeList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

