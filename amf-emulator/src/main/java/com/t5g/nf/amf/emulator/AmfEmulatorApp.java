package com.t5g.nf.amf.emulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AmfEmulatorApp {


    public static void main(String[] args) throws Exception {
        new SpringApplication(AmfEmulatorApp.class).run(args);
    }


}
