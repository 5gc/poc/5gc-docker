package com.t5g.nf.ausf.ueauthentication.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ResynchronizationInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-20T12:28:34.805+02:00[Europe/Helsinki]")

public class ResynchronizationInfo   {
  @JsonProperty("rand")
  private String rand;

  @JsonProperty("auts")
  private String auts;

  public ResynchronizationInfo rand(String rand) {
    this.rand = rand;
    return this;
  }

  /**
   * Get rand
   * @return rand
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]{32}$") 
  public String getRand() {
    return rand;
  }

  public void setRand(String rand) {
    this.rand = rand;
  }

  public ResynchronizationInfo auts(String auts) {
    this.auts = auts;
    return this;
  }

  /**
   * Get auts
   * @return auts
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Fa-f0-9]{28}$") 
  public String getAuts() {
    return auts;
  }

  public void setAuts(String auts) {
    this.auts = auts;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResynchronizationInfo resynchronizationInfo = (ResynchronizationInfo) o;
    return Objects.equals(this.rand, resynchronizationInfo.rand) &&
        Objects.equals(this.auts, resynchronizationInfo.auts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rand, auts);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResynchronizationInfo {\n");
    
    sb.append("    rand: ").append(toIndentedString(rand)).append("\n");
    sb.append("    auts: ").append(toIndentedString(auts)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

