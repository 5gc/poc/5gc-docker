package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.AmfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.AusfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.BsfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.ChfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.DefaultNotificationSubscription;
import com.t5g.nf.nrf.nfdiscovery.model.NFService;
import com.t5g.nf.nrf.nfdiscovery.model.NFStatus;
import com.t5g.nf.nrf.nfdiscovery.model.NFType;
import com.t5g.nf.nrf.nfdiscovery.model.PcfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnId;
import com.t5g.nf.nrf.nfdiscovery.model.PlmnSnssai;
import com.t5g.nf.nrf.nfdiscovery.model.SmfInfo;
import com.t5g.nf.nrf.nfdiscovery.model.Snssai;
import com.t5g.nf.nrf.nfdiscovery.model.UdmInfo;
import com.t5g.nf.nrf.nfdiscovery.model.UdrInfo;
import com.t5g.nf.nrf.nfdiscovery.model.UpfInfo;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NFProfile
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class NFProfile   {
  @JsonProperty("nfInstanceId")
  private UUID nfInstanceId;

  @JsonProperty("nfType")
  private NFType nfType = null;

  @JsonProperty("nfStatus")
  private NFStatus nfStatus = null;

  @JsonProperty("plmnList")
  @Valid
  private List<PlmnId> plmnList = null;

  @JsonProperty("sNssais")
  @Valid
  private List<Snssai> sNssais = null;

  @JsonProperty("perPlmnSnssaiList")
  @Valid
  private List<PlmnSnssai> perPlmnSnssaiList = null;

  @JsonProperty("nsiList")
  @Valid
  private List<String> nsiList = null;

  @JsonProperty("fqdn")
  private String fqdn;

  @JsonProperty("ipv4Addresses")
  @Valid
  private List<String> ipv4Addresses = null;

  @JsonProperty("ipv6Addresses")
  @Valid
  private List<String> ipv6Addresses = null;

  @JsonProperty("capacity")
  private Integer capacity;

  @JsonProperty("load")
  private Integer load;

  @JsonProperty("locality")
  private String locality;

  @JsonProperty("priority")
  private Integer priority;

  @JsonProperty("udrInfo")
  private UdrInfo udrInfo;

  @JsonProperty("udmInfo")
  private UdmInfo udmInfo;

  @JsonProperty("ausfInfo")
  private AusfInfo ausfInfo;

  @JsonProperty("amfInfo")
  private AmfInfo amfInfo;

  @JsonProperty("smfInfo")
  private SmfInfo smfInfo;

  @JsonProperty("upfInfo")
  private UpfInfo upfInfo;

  @JsonProperty("pcfInfo")
  private PcfInfo pcfInfo;

  @JsonProperty("bsfInfo")
  private BsfInfo bsfInfo;

  @JsonProperty("chfInfo")
  private ChfInfo chfInfo;

  @JsonProperty("customInfo")
  private Object customInfo;

  @JsonProperty("recoveryTime")
  private OffsetDateTime recoveryTime;

  @JsonProperty("nfServicePersistence")
  private Boolean nfServicePersistence = false;

  @JsonProperty("nfServices")
  @Valid
  private List<NFService> nfServices = null;

  @JsonProperty("defaultNotificationSubscriptions")
  @Valid
  private List<DefaultNotificationSubscription> defaultNotificationSubscriptions = null;

  public NFProfile nfInstanceId(UUID nfInstanceId) {
    this.nfInstanceId = nfInstanceId;
    return this;
  }

  /**
   * Get nfInstanceId
   * @return nfInstanceId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public UUID getNfInstanceId() {
    return nfInstanceId;
  }

  public void setNfInstanceId(UUID nfInstanceId) {
    this.nfInstanceId = nfInstanceId;
  }

  public NFProfile nfType(NFType nfType) {
    this.nfType = nfType;
    return this;
  }

  /**
   * Get nfType
   * @return nfType
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public NFType getNfType() {
    return nfType;
  }

  public void setNfType(NFType nfType) {
    this.nfType = nfType;
  }

  public NFProfile nfStatus(NFStatus nfStatus) {
    this.nfStatus = nfStatus;
    return this;
  }

  /**
   * Get nfStatus
   * @return nfStatus
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public NFStatus getNfStatus() {
    return nfStatus;
  }

  public void setNfStatus(NFStatus nfStatus) {
    this.nfStatus = nfStatus;
  }

  public NFProfile plmnList(List<PlmnId> plmnList) {
    this.plmnList = plmnList;
    return this;
  }

  public NFProfile addPlmnListItem(PlmnId plmnListItem) {
    if (this.plmnList == null) {
      this.plmnList = new ArrayList<>();
    }
    this.plmnList.add(plmnListItem);
    return this;
  }

  /**
   * Get plmnList
   * @return plmnList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<PlmnId> getPlmnList() {
    return plmnList;
  }

  public void setPlmnList(List<PlmnId> plmnList) {
    this.plmnList = plmnList;
  }

  public NFProfile sNssais(List<Snssai> sNssais) {
    this.sNssais = sNssais;
    return this;
  }

  public NFProfile addSNssaisItem(Snssai sNssaisItem) {
    if (this.sNssais == null) {
      this.sNssais = new ArrayList<>();
    }
    this.sNssais.add(sNssaisItem);
    return this;
  }

  /**
   * Get sNssais
   * @return sNssais
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<Snssai> getsNssais() {
    return sNssais;
  }

  public void setsNssais(List<Snssai> sNssais) {
    this.sNssais = sNssais;
  }

  public NFProfile perPlmnSnssaiList(List<PlmnSnssai> perPlmnSnssaiList) {
    this.perPlmnSnssaiList = perPlmnSnssaiList;
    return this;
  }

  public NFProfile addPerPlmnSnssaiListItem(PlmnSnssai perPlmnSnssaiListItem) {
    if (this.perPlmnSnssaiList == null) {
      this.perPlmnSnssaiList = new ArrayList<>();
    }
    this.perPlmnSnssaiList.add(perPlmnSnssaiListItem);
    return this;
  }

  /**
   * Get perPlmnSnssaiList
   * @return perPlmnSnssaiList
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<PlmnSnssai> getPerPlmnSnssaiList() {
    return perPlmnSnssaiList;
  }

  public void setPerPlmnSnssaiList(List<PlmnSnssai> perPlmnSnssaiList) {
    this.perPlmnSnssaiList = perPlmnSnssaiList;
  }

  public NFProfile nsiList(List<String> nsiList) {
    this.nsiList = nsiList;
    return this;
  }

  public NFProfile addNsiListItem(String nsiListItem) {
    if (this.nsiList == null) {
      this.nsiList = new ArrayList<>();
    }
    this.nsiList.add(nsiListItem);
    return this;
  }

  /**
   * Get nsiList
   * @return nsiList
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getNsiList() {
    return nsiList;
  }

  public void setNsiList(List<String> nsiList) {
    this.nsiList = nsiList;
  }

  public NFProfile fqdn(String fqdn) {
    this.fqdn = fqdn;
    return this;
  }

  /**
   * Get fqdn
   * @return fqdn
  */
  @ApiModelProperty(value = "")


  public String getFqdn() {
    return fqdn;
  }

  public void setFqdn(String fqdn) {
    this.fqdn = fqdn;
  }

  public NFProfile ipv4Addresses(List<String> ipv4Addresses) {
    this.ipv4Addresses = ipv4Addresses;
    return this;
  }

  public NFProfile addIpv4AddressesItem(String ipv4AddressesItem) {
    if (this.ipv4Addresses == null) {
      this.ipv4Addresses = new ArrayList<>();
    }
    this.ipv4Addresses.add(ipv4AddressesItem);
    return this;
  }

  /**
   * Get ipv4Addresses
   * @return ipv4Addresses
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getIpv4Addresses() {
    return ipv4Addresses;
  }

  public void setIpv4Addresses(List<String> ipv4Addresses) {
    this.ipv4Addresses = ipv4Addresses;
  }

  public NFProfile ipv6Addresses(List<String> ipv6Addresses) {
    this.ipv6Addresses = ipv6Addresses;
    return this;
  }

  public NFProfile addIpv6AddressesItem(String ipv6AddressesItem) {
    if (this.ipv6Addresses == null) {
      this.ipv6Addresses = new ArrayList<>();
    }
    this.ipv6Addresses.add(ipv6AddressesItem);
    return this;
  }

  /**
   * Get ipv6Addresses
   * @return ipv6Addresses
  */
  @ApiModelProperty(value = "")

@Size(min=1) 
  public List<String> getIpv6Addresses() {
    return ipv6Addresses;
  }

  public void setIpv6Addresses(List<String> ipv6Addresses) {
    this.ipv6Addresses = ipv6Addresses;
  }

  public NFProfile capacity(Integer capacity) {
    this.capacity = capacity;
    return this;
  }

  /**
   * Get capacity
   * minimum: 0
   * maximum: 65535
   * @return capacity
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(65535) 
  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public NFProfile load(Integer load) {
    this.load = load;
    return this;
  }

  /**
   * Get load
   * minimum: 0
   * maximum: 100
   * @return load
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(100) 
  public Integer getLoad() {
    return load;
  }

  public void setLoad(Integer load) {
    this.load = load;
  }

  public NFProfile locality(String locality) {
    this.locality = locality;
    return this;
  }

  /**
   * Get locality
   * @return locality
  */
  @ApiModelProperty(value = "")


  public String getLocality() {
    return locality;
  }

  public void setLocality(String locality) {
    this.locality = locality;
  }

  public NFProfile priority(Integer priority) {
    this.priority = priority;
    return this;
  }

  /**
   * Get priority
   * minimum: 0
   * maximum: 65535
   * @return priority
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(65535) 
  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public NFProfile udrInfo(UdrInfo udrInfo) {
    this.udrInfo = udrInfo;
    return this;
  }

  /**
   * Get udrInfo
   * @return udrInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public UdrInfo getUdrInfo() {
    return udrInfo;
  }

  public void setUdrInfo(UdrInfo udrInfo) {
    this.udrInfo = udrInfo;
  }

  public NFProfile udmInfo(UdmInfo udmInfo) {
    this.udmInfo = udmInfo;
    return this;
  }

  /**
   * Get udmInfo
   * @return udmInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public UdmInfo getUdmInfo() {
    return udmInfo;
  }

  public void setUdmInfo(UdmInfo udmInfo) {
    this.udmInfo = udmInfo;
  }

  public NFProfile ausfInfo(AusfInfo ausfInfo) {
    this.ausfInfo = ausfInfo;
    return this;
  }

  /**
   * Get ausfInfo
   * @return ausfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public AusfInfo getAusfInfo() {
    return ausfInfo;
  }

  public void setAusfInfo(AusfInfo ausfInfo) {
    this.ausfInfo = ausfInfo;
  }

  public NFProfile amfInfo(AmfInfo amfInfo) {
    this.amfInfo = amfInfo;
    return this;
  }

  /**
   * Get amfInfo
   * @return amfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public AmfInfo getAmfInfo() {
    return amfInfo;
  }

  public void setAmfInfo(AmfInfo amfInfo) {
    this.amfInfo = amfInfo;
  }

  public NFProfile smfInfo(SmfInfo smfInfo) {
    this.smfInfo = smfInfo;
    return this;
  }

  /**
   * Get smfInfo
   * @return smfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public SmfInfo getSmfInfo() {
    return smfInfo;
  }

  public void setSmfInfo(SmfInfo smfInfo) {
    this.smfInfo = smfInfo;
  }

  public NFProfile upfInfo(UpfInfo upfInfo) {
    this.upfInfo = upfInfo;
    return this;
  }

  /**
   * Get upfInfo
   * @return upfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public UpfInfo getUpfInfo() {
    return upfInfo;
  }

  public void setUpfInfo(UpfInfo upfInfo) {
    this.upfInfo = upfInfo;
  }

  public NFProfile pcfInfo(PcfInfo pcfInfo) {
    this.pcfInfo = pcfInfo;
    return this;
  }

  /**
   * Get pcfInfo
   * @return pcfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public PcfInfo getPcfInfo() {
    return pcfInfo;
  }

  public void setPcfInfo(PcfInfo pcfInfo) {
    this.pcfInfo = pcfInfo;
  }

  public NFProfile bsfInfo(BsfInfo bsfInfo) {
    this.bsfInfo = bsfInfo;
    return this;
  }

  /**
   * Get bsfInfo
   * @return bsfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public BsfInfo getBsfInfo() {
    return bsfInfo;
  }

  public void setBsfInfo(BsfInfo bsfInfo) {
    this.bsfInfo = bsfInfo;
  }

  public NFProfile chfInfo(ChfInfo chfInfo) {
    this.chfInfo = chfInfo;
    return this;
  }

  /**
   * Get chfInfo
   * @return chfInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public ChfInfo getChfInfo() {
    return chfInfo;
  }

  public void setChfInfo(ChfInfo chfInfo) {
    this.chfInfo = chfInfo;
  }

  public NFProfile customInfo(Object customInfo) {
    this.customInfo = customInfo;
    return this;
  }

  /**
   * Get customInfo
   * @return customInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public Object getCustomInfo() {
    return customInfo;
  }

  public void setCustomInfo(Object customInfo) {
    this.customInfo = customInfo;
  }

  public NFProfile recoveryTime(OffsetDateTime recoveryTime) {
    this.recoveryTime = recoveryTime;
    return this;
  }

  /**
   * Get recoveryTime
   * @return recoveryTime
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getRecoveryTime() {
    return recoveryTime;
  }

  public void setRecoveryTime(OffsetDateTime recoveryTime) {
    this.recoveryTime = recoveryTime;
  }

  public NFProfile nfServicePersistence(Boolean nfServicePersistence) {
    this.nfServicePersistence = nfServicePersistence;
    return this;
  }

  /**
   * Get nfServicePersistence
   * @return nfServicePersistence
  */
  @ApiModelProperty(value = "")


  public Boolean getNfServicePersistence() {
    return nfServicePersistence;
  }

  public void setNfServicePersistence(Boolean nfServicePersistence) {
    this.nfServicePersistence = nfServicePersistence;
  }

  public NFProfile nfServices(List<NFService> nfServices) {
    this.nfServices = nfServices;
    return this;
  }

  public NFProfile addNfServicesItem(NFService nfServicesItem) {
    if (this.nfServices == null) {
      this.nfServices = new ArrayList<>();
    }
    this.nfServices.add(nfServicesItem);
    return this;
  }

  /**
   * Get nfServices
   * @return nfServices
  */
  @ApiModelProperty(value = "")

  @Valid
@Size(min=1) 
  public List<NFService> getNfServices() {
    return nfServices;
  }

  public void setNfServices(List<NFService> nfServices) {
    this.nfServices = nfServices;
  }

  public NFProfile defaultNotificationSubscriptions(List<DefaultNotificationSubscription> defaultNotificationSubscriptions) {
    this.defaultNotificationSubscriptions = defaultNotificationSubscriptions;
    return this;
  }

  public NFProfile addDefaultNotificationSubscriptionsItem(DefaultNotificationSubscription defaultNotificationSubscriptionsItem) {
    if (this.defaultNotificationSubscriptions == null) {
      this.defaultNotificationSubscriptions = new ArrayList<>();
    }
    this.defaultNotificationSubscriptions.add(defaultNotificationSubscriptionsItem);
    return this;
  }

  /**
   * Get defaultNotificationSubscriptions
   * @return defaultNotificationSubscriptions
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<DefaultNotificationSubscription> getDefaultNotificationSubscriptions() {
    return defaultNotificationSubscriptions;
  }

  public void setDefaultNotificationSubscriptions(List<DefaultNotificationSubscription> defaultNotificationSubscriptions) {
    this.defaultNotificationSubscriptions = defaultNotificationSubscriptions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NFProfile nfProfile = (NFProfile) o;
    return Objects.equals(this.nfInstanceId, nfProfile.nfInstanceId) &&
        Objects.equals(this.nfType, nfProfile.nfType) &&
        Objects.equals(this.nfStatus, nfProfile.nfStatus) &&
        Objects.equals(this.plmnList, nfProfile.plmnList) &&
        Objects.equals(this.sNssais, nfProfile.sNssais) &&
        Objects.equals(this.perPlmnSnssaiList, nfProfile.perPlmnSnssaiList) &&
        Objects.equals(this.nsiList, nfProfile.nsiList) &&
        Objects.equals(this.fqdn, nfProfile.fqdn) &&
        Objects.equals(this.ipv4Addresses, nfProfile.ipv4Addresses) &&
        Objects.equals(this.ipv6Addresses, nfProfile.ipv6Addresses) &&
        Objects.equals(this.capacity, nfProfile.capacity) &&
        Objects.equals(this.load, nfProfile.load) &&
        Objects.equals(this.locality, nfProfile.locality) &&
        Objects.equals(this.priority, nfProfile.priority) &&
        Objects.equals(this.udrInfo, nfProfile.udrInfo) &&
        Objects.equals(this.udmInfo, nfProfile.udmInfo) &&
        Objects.equals(this.ausfInfo, nfProfile.ausfInfo) &&
        Objects.equals(this.amfInfo, nfProfile.amfInfo) &&
        Objects.equals(this.smfInfo, nfProfile.smfInfo) &&
        Objects.equals(this.upfInfo, nfProfile.upfInfo) &&
        Objects.equals(this.pcfInfo, nfProfile.pcfInfo) &&
        Objects.equals(this.bsfInfo, nfProfile.bsfInfo) &&
        Objects.equals(this.chfInfo, nfProfile.chfInfo) &&
        Objects.equals(this.customInfo, nfProfile.customInfo) &&
        Objects.equals(this.recoveryTime, nfProfile.recoveryTime) &&
        Objects.equals(this.nfServicePersistence, nfProfile.nfServicePersistence) &&
        Objects.equals(this.nfServices, nfProfile.nfServices) &&
        Objects.equals(this.defaultNotificationSubscriptions, nfProfile.defaultNotificationSubscriptions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nfInstanceId, nfType, nfStatus, plmnList, sNssais, perPlmnSnssaiList, nsiList, fqdn, ipv4Addresses, ipv6Addresses, capacity, load, locality, priority, udrInfo, udmInfo, ausfInfo, amfInfo, smfInfo, upfInfo, pcfInfo, bsfInfo, chfInfo, customInfo, recoveryTime, nfServicePersistence, nfServices, defaultNotificationSubscriptions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NFProfile {\n");
    
    sb.append("    nfInstanceId: ").append(toIndentedString(nfInstanceId)).append("\n");
    sb.append("    nfType: ").append(toIndentedString(nfType)).append("\n");
    sb.append("    nfStatus: ").append(toIndentedString(nfStatus)).append("\n");
    sb.append("    plmnList: ").append(toIndentedString(plmnList)).append("\n");
    sb.append("    sNssais: ").append(toIndentedString(sNssais)).append("\n");
    sb.append("    perPlmnSnssaiList: ").append(toIndentedString(perPlmnSnssaiList)).append("\n");
    sb.append("    nsiList: ").append(toIndentedString(nsiList)).append("\n");
    sb.append("    fqdn: ").append(toIndentedString(fqdn)).append("\n");
    sb.append("    ipv4Addresses: ").append(toIndentedString(ipv4Addresses)).append("\n");
    sb.append("    ipv6Addresses: ").append(toIndentedString(ipv6Addresses)).append("\n");
    sb.append("    capacity: ").append(toIndentedString(capacity)).append("\n");
    sb.append("    load: ").append(toIndentedString(load)).append("\n");
    sb.append("    locality: ").append(toIndentedString(locality)).append("\n");
    sb.append("    priority: ").append(toIndentedString(priority)).append("\n");
    sb.append("    udrInfo: ").append(toIndentedString(udrInfo)).append("\n");
    sb.append("    udmInfo: ").append(toIndentedString(udmInfo)).append("\n");
    sb.append("    ausfInfo: ").append(toIndentedString(ausfInfo)).append("\n");
    sb.append("    amfInfo: ").append(toIndentedString(amfInfo)).append("\n");
    sb.append("    smfInfo: ").append(toIndentedString(smfInfo)).append("\n");
    sb.append("    upfInfo: ").append(toIndentedString(upfInfo)).append("\n");
    sb.append("    pcfInfo: ").append(toIndentedString(pcfInfo)).append("\n");
    sb.append("    bsfInfo: ").append(toIndentedString(bsfInfo)).append("\n");
    sb.append("    chfInfo: ").append(toIndentedString(chfInfo)).append("\n");
    sb.append("    customInfo: ").append(toIndentedString(customInfo)).append("\n");
    sb.append("    recoveryTime: ").append(toIndentedString(recoveryTime)).append("\n");
    sb.append("    nfServicePersistence: ").append(toIndentedString(nfServicePersistence)).append("\n");
    sb.append("    nfServices: ").append(toIndentedString(nfServices)).append("\n");
    sb.append("    defaultNotificationSubscriptions: ").append(toIndentedString(defaultNotificationSubscriptions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

