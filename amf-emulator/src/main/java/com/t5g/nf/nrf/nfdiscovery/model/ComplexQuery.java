package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.Cnf;
import com.t5g.nf.nrf.nfdiscovery.model.CnfUnit;
import com.t5g.nf.nrf.nfdiscovery.model.Dnf;
import com.t5g.nf.nrf.nfdiscovery.model.DnfUnit;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ComplexQuery
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class ComplexQuery   {
  @JsonProperty("cnfUnits")
  @Valid
  private List<CnfUnit> cnfUnits = new ArrayList<>();

  @JsonProperty("dnfUnits")
  @Valid
  private List<DnfUnit> dnfUnits = new ArrayList<>();

  public ComplexQuery cnfUnits(List<CnfUnit> cnfUnits) {
    this.cnfUnits = cnfUnits;
    return this;
  }

  public ComplexQuery addCnfUnitsItem(CnfUnit cnfUnitsItem) {
    this.cnfUnits.add(cnfUnitsItem);
    return this;
  }

  /**
   * Get cnfUnits
   * @return cnfUnits
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<CnfUnit> getCnfUnits() {
    return cnfUnits;
  }

  public void setCnfUnits(List<CnfUnit> cnfUnits) {
    this.cnfUnits = cnfUnits;
  }

  public ComplexQuery dnfUnits(List<DnfUnit> dnfUnits) {
    this.dnfUnits = dnfUnits;
    return this;
  }

  public ComplexQuery addDnfUnitsItem(DnfUnit dnfUnitsItem) {
    this.dnfUnits.add(dnfUnitsItem);
    return this;
  }

  /**
   * Get dnfUnits
   * @return dnfUnits
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
@Size(min=1) 
  public List<DnfUnit> getDnfUnits() {
    return dnfUnits;
  }

  public void setDnfUnits(List<DnfUnit> dnfUnits) {
    this.dnfUnits = dnfUnits;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ComplexQuery complexQuery = (ComplexQuery) o;
    return Objects.equals(this.cnfUnits, complexQuery.cnfUnits) &&
        Objects.equals(this.dnfUnits, complexQuery.dnfUnits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cnfUnits, dnfUnits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ComplexQuery {\n");
    
    sb.append("    cnfUnits: ").append(toIndentedString(cnfUnits)).append("\n");
    sb.append("    dnfUnits: ").append(toIndentedString(dnfUnits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

