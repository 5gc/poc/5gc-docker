package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.t5g.nf.nrf.nfdiscovery.model.TransportProtocol;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * IpEndPoint
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class IpEndPoint   {
  @JsonProperty("ipv4Address")
  private String ipv4Address;

  @JsonProperty("ipv6Address")
  private String ipv6Address;

  @JsonProperty("transport")
  private TransportProtocol transport = null;

  @JsonProperty("port")
  private Integer port;

  public IpEndPoint ipv4Address(String ipv4Address) {
    this.ipv4Address = ipv4Address;
    return this;
  }

  /**
   * Get ipv4Address
   * @return ipv4Address
  */
  @ApiModelProperty(example = "198.51.100.1", value = "")

@Pattern(regexp="^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$") 
  public String getIpv4Address() {
    return ipv4Address;
  }

  public void setIpv4Address(String ipv4Address) {
    this.ipv4Address = ipv4Address;
  }

  public IpEndPoint ipv6Address(String ipv6Address) {
    this.ipv6Address = ipv6Address;
    return this;
  }

  /**
   * Get ipv6Address
   * @return ipv6Address
  */
  @ApiModelProperty(value = "")


  public String getIpv6Address() {
    return ipv6Address;
  }

  public void setIpv6Address(String ipv6Address) {
    this.ipv6Address = ipv6Address;
  }

  public IpEndPoint transport(TransportProtocol transport) {
    this.transport = transport;
    return this;
  }

  /**
   * Get transport
   * @return transport
  */
  @ApiModelProperty(value = "")

  @Valid

  public TransportProtocol getTransport() {
    return transport;
  }

  public void setTransport(TransportProtocol transport) {
    this.transport = transport;
  }

  public IpEndPoint port(Integer port) {
    this.port = port;
    return this;
  }

  /**
   * Get port
   * minimum: 0
   * maximum: 65535
   * @return port
  */
  @ApiModelProperty(value = "")

@Min(0) @Max(65535) 
  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IpEndPoint ipEndPoint = (IpEndPoint) o;
    return Objects.equals(this.ipv4Address, ipEndPoint.ipv4Address) &&
        Objects.equals(this.ipv6Address, ipEndPoint.ipv6Address) &&
        Objects.equals(this.transport, ipEndPoint.transport) &&
        Objects.equals(this.port, ipEndPoint.port);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ipv4Address, ipv6Address, transport, port);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IpEndPoint {\n");
    
    sb.append("    ipv4Address: ").append(toIndentedString(ipv4Address)).append("\n");
    sb.append("    ipv6Address: ").append(toIndentedString(ipv6Address)).append("\n");
    sb.append("    transport: ").append(toIndentedString(transport)).append("\n");
    sb.append("    port: ").append(toIndentedString(port)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

