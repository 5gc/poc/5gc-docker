package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Atom
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class Atom   {
  @JsonProperty("attr")
  private String attr;

  @JsonProperty("value")
  private Object value = null;

  @JsonProperty("negative")
  private Boolean negative;

  public Atom attr(String attr) {
    this.attr = attr;
    return this;
  }

  /**
   * Get attr
   * @return attr
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getAttr() {
    return attr;
  }

  public void setAttr(String attr) {
    this.attr = attr;
  }

  public Atom value(Object value) {
    this.value = value;
    return this;
  }

  /**
   * Get value
   * @return value
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public Atom negative(Boolean negative) {
    this.negative = negative;
    return this;
  }

  /**
   * Get negative
   * @return negative
  */
  @ApiModelProperty(value = "")


  public Boolean getNegative() {
    return negative;
  }

  public void setNegative(Boolean negative) {
    this.negative = negative;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Atom atom = (Atom) o;
    return Objects.equals(this.attr, atom.attr) &&
        Objects.equals(this.value, atom.value) &&
        Objects.equals(this.negative, atom.negative);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attr, value, negative);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Atom {\n");
    
    sb.append("    attr: ").append(toIndentedString(attr)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    negative: ").append(toIndentedString(negative)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

