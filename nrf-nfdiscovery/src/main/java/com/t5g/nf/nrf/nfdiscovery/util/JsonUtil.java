package com.t5g.nf.nrf.nfdiscovery.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonUtil {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	public static <T> T toObject(String json, Class<T> type) throws IOException {
		return OBJECT_MAPPER.readValue(json, type);
	}

}
