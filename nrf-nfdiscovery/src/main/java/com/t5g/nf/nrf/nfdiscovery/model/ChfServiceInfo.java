package com.t5g.nf.nrf.nfdiscovery.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChfServiceInfo
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-12-12T13:04:22.899+02:00[Europe/Helsinki]")

public class ChfServiceInfo   {
  @JsonProperty("primaryChfServiceInstance")
  private String primaryChfServiceInstance;

  @JsonProperty("secondaryChfServiceInstance")
  private String secondaryChfServiceInstance;

  public ChfServiceInfo primaryChfServiceInstance(String primaryChfServiceInstance) {
    this.primaryChfServiceInstance = primaryChfServiceInstance;
    return this;
  }

  /**
   * Get primaryChfServiceInstance
   * @return primaryChfServiceInstance
  */
  @ApiModelProperty(value = "")


  public String getPrimaryChfServiceInstance() {
    return primaryChfServiceInstance;
  }

  public void setPrimaryChfServiceInstance(String primaryChfServiceInstance) {
    this.primaryChfServiceInstance = primaryChfServiceInstance;
  }

  public ChfServiceInfo secondaryChfServiceInstance(String secondaryChfServiceInstance) {
    this.secondaryChfServiceInstance = secondaryChfServiceInstance;
    return this;
  }

  /**
   * Get secondaryChfServiceInstance
   * @return secondaryChfServiceInstance
  */
  @ApiModelProperty(value = "")


  public String getSecondaryChfServiceInstance() {
    return secondaryChfServiceInstance;
  }

  public void setSecondaryChfServiceInstance(String secondaryChfServiceInstance) {
    this.secondaryChfServiceInstance = secondaryChfServiceInstance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChfServiceInfo chfServiceInfo = (ChfServiceInfo) o;
    return Objects.equals(this.primaryChfServiceInstance, chfServiceInfo.primaryChfServiceInstance) &&
        Objects.equals(this.secondaryChfServiceInstance, chfServiceInfo.secondaryChfServiceInstance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(primaryChfServiceInstance, secondaryChfServiceInstance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChfServiceInfo {\n");
    
    sb.append("    primaryChfServiceInstance: ").append(toIndentedString(primaryChfServiceInstance)).append("\n");
    sb.append("    secondaryChfServiceInstance: ").append(toIndentedString(secondaryChfServiceInstance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

